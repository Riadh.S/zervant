export const typeOfCustomer = [
  {
    key: "New company customer",
    text: "New company customer",
    value: "New company customer"
  },
  {
    key: "New private customer",
    text: "New private customer",
    value: "New private customer"
  }
];
export const invoicingLanguage = [
  { key: "French", text: "French", value: "French" },
  { key: "English", text: "English", value: "English" }
];

export const paymentTerm = [
  { key: "30", text: "30", value: "30" },
  { key: "60", text: "60", value: "60" },
  { key: "90", text: "90", value: "90" }
];

export const eInvoiceOperator = ["4 solutions", "3E consultancy solustions"];

export const eInvoiceNetworkType = [
  "public PEPPOL network",
  "Private Operator Network"
];
export function getCustomerDefault() {
  return {
    typeOfCustomer: typeOfCustomer[0],
    invoicingLanguage: invoicingLanguage[0],
    paymentTerm: paymentTerm[0],
    eInvoiceOperator: eInvoiceOperator[0],
    eInvoiceNetworkType: eInvoiceNetworkType[0]
  };
}
