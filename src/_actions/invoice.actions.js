import { Service } from '../_services/';
import {FETECHED_ALL_INVOICE,ID_PDF,CHANGE_DATE,DELETE_ROW,ADD_ROW_DETAIL_INVOICE,PUT_DETAIL_INVOICE,CHANGE_DATE_DETAIL_INVOICE_PRODUCT,CHANGE_DETAIL_INVOICE_PRODUCTS,ADD_INVOICE,CHANGE_DATE11,CHANGE_DETAIL_INVOICE,CHANGE_DATE22,ADD_ROW_PRODUCT,EDIT_INVOICE,NEW_INVOICE,CHANGE_DATA_INVOICE_PRODUCTS,FETCH_START,ORDER_INVOICES,CHANGE_DATA_INVOICE,SEARCH_INVOICES,DELETE_INVOICE,DETAIL_INVOICES,PAGINATION_INVOICE,UPDATE_INVOICE,UPDATE_INVOICE_FAILED,CHANGE_VALUE_INVOICE, CHANGE_DATA_DEVISSETTING, DELETE_ROW_DETAIL_INVOICE_PRODUCT} from './types';
import { getIdUser } from '../_services/UserServices';
import { saveAs } from 'file-saver';
export const invoiceActions = {
    getInvoices,
    orderInvoices,
    searchInvoice,
    detailInvoice,
    invoicePagination,
    updateInvoice,
    changeValueInvoice,
    deleteInvoice,
    changeDataInvoice,
    addInvoice,
    generatePdf,
    getPDF,
    changeDataInvoiceProducts,
    addRowProduct,
    newInvoice,
    changeDate,
    deleteRow,
    editDataInvoice,
    sendEmail,
    changeDate1,
    changeDate2,
    changeDetailInvoice,
    addRowProductDetailInvoice,
    changeDetailInvoiceProducts,
    deleteRowDetailInvoice,
    changeDateDetailInvoiceProduct,
    updateStatusInvoice
};

function getInvoices(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'bills/'+id;
        dispatch(fetchStarted());
        Service.get(apiEndpoint)
        .then((response)=>{
            let invoices=response.data.data;
            let apiEndpoint = 'products/'+id;
            Service.get(apiEndpoint)
             .then((response)=>{
                let products=response.data.data;
                let apiEndpoint = 'customers/'+id;
                Service.get(apiEndpoint)
                .then((response)=>{
                    let customers=response.data.data;
                    console.log(response.data.data); 
                       dispatch(invoicesList(invoices,products,customers));
                })
             })    
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function changeDetailInvoiceProducts(name,value,index,products){
    return dispatch=>{
        dispatch(changeDetailInvProducts(name,value,index,products));
    }
}
function updateStatusInvoice(invoice,id){
    return dispatch=>{
        let apiEndpoint='bills/'+id;
        let payload=invoice;
        Service.put(apiEndpoint,payload)
        .then((response)=>{
        if(response.status===200){
            console.log(response.data.data);
            dispatch(putDetailInvoice(response.data.data,invoice));
           
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(putInvoiceFailed());
        })
        
    }
}
function addRowProductDetailInvoice(){
    return dispatch=>{
        dispatch(addRowDetailInvoice());
    }
}
function deleteRowDetailInvoice(val){
    return dispatch=>{
        dispatch(deleteRowDetailInv(val))
    }
}
function deleteRow(val){
    console.log("++++++"+val);
    return dispatch=>{
        dispatch(deleteNewRow(val));
    }
}
function changeDetailInvoice(name,value){
    return dispatch=>{
        dispatch(changeDetailInv(name,value));
    }
}
function changeDate1(date){
    return dispatch=>{
        dispatch(updateDate1(date));
    }
}
function changeDate2(date){
    return dispatch=>{
        dispatch(updateDate2(date));
    }
}
function newInvoice(data){
    console.log(data);
    return dispatch=>{
        dispatch(addInvoiceData(data));
    }
}
function sendEmail(invoice){
    
        let apiEndpoint = 'bills/send-email';
        let payload=invoice;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            console.log(response.data.message);
            //dispatch(sendEmailSuccess(response.data.message));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })

}
function changeDate(date,val){
    return dispatch=>{
        dispatch(changeDateProduct(date,val));
    }
}
function changeDateDetailInvoiceProduct(date,val){
    return dispatch=>{
        dispatch(changeDateDetailInvProduct(date,val));
    }
}
function editDataInvoice(){
    return dispatch=>{
        dispatch(editInv());
    }
}
function addRowProduct(){
    return dispatch=>{
     dispatch(addRow());
}
}
function generatePdf(invoice){
    return dispatch=>{
    let apiEndpoint = 'bills/create-pdf';
        let payload=invoice;
        Service.post(apiEndpoint,payload)

        .then((res) =>{
        if(res.data.data){
            console.log(res.data.data);
          dispatch(getIdPdf(res.data.data)); 
        }
    })
   .catch((err)=>{
    console.log(err);
   });
}
}
function getPDF(){
    let apiEndpoint = 'bills/fetch-pdf';
    Service.getpdf(apiEndpoint)
    .then((res) => { 
        
        const pdfBlob = new Blob([res.data], { type: 'application/pdf' });
        console.log(pdfBlob);
        saveAs(pdfBlob, 'invoice.pdf');
    }
      
)
.catch((err)=>console.log(err));
}
function addInvoice(invoice){
    return dispatch => {
        let apiEndpoint = 'bills';
        let payload=invoice;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            dispatch(addNewInvoice(invoice));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    }
}
function changeDataInvoiceProducts(name,value,index,products){
    console.log(products);
    return dispatch=>{
        dispatch(changeInvoiceProducts(name,value,index,products));
    }
}
function orderInvoices(val){

    return dispatch=>{
        dispatch(orderInvoicesList(val));
    }
}
function changeValueInvoice(name,value){
    return dispatch=>{
        dispatch(changeInvoice(name,value));
    }
}
function changeDataInvoice(name,value){
    return dispatch=>{
        dispatch(dataInvoice(name,value));
    }
}
function searchInvoice(invoiceName){
    return dispatch=>{
        dispatch(searchInvoicesList(invoiceName));
    }
}
function invoicePagination(nbrPage){
    console.log(nbrPage);
    return dispatch=>{
        dispatch(paginationInvoicesList(nbrPage));
    }
}
function detailInvoice(invoice){
    return dispatch=>{
        dispatch(detailInvoicesList(invoice));
    }
}
function updateInvoice(invoice,id){
    return dispatch=>{
        let apiEndpoint = 'bills/update-pdf/'+id;
        let payload =invoice;
        console.log(payload);
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            if(response.status===200){
            dispatch(putInvoice(invoice));
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(putInvoiceFailed());
        })
    }
}
function deleteInvoice(id){
    return dispatch =>{
        let apiEndpoint = 'bills/'+id;
        Service.deleteDetail(apiEndpoint)
        .then((res)=>{
            if(res.status===200){
                dispatch(deleteOneInvoice(id));
            }
        })
        .catch((err)=>console.log(err))
    }
}
export function editInv(){
    return {
        type:EDIT_INVOICE
    }
}
export function changeDetailInv(name,value){
    return{
        type:CHANGE_DETAIL_INVOICE,
        name:name,
        value:value
    }
}
export function putDetailInvoice(newData,invoice){
    console.log(invoice);
    return{
        type:PUT_DETAIL_INVOICE,
        invoice:invoice,
        newData:newData
    }
}
export function updateDate1(date){
    return{
        type:CHANGE_DATE11,
        date:date
    }
 }
 export function addRowDetailInvoice(){
     return {
         type:ADD_ROW_DETAIL_INVOICE
     }
 }
 export function deleteRowDetailInv(val){
     return{
         type:DELETE_ROW_DETAIL_INVOICE_PRODUCT,
         val:val
     }
 }
 export function updateDate2(date){
     return{
         type:CHANGE_DATE22,
         date:date
     }
 }
export function invoicesList(invoices,products,customers){
    return{
        type: FETECHED_ALL_INVOICE,
        invoices: invoices,
        products:products,
        customers:customers
    }
}
/*export function noChangeInvoicesList(){
    return{
        type:INVOICE_NOT_FOUND
    }
}*/
export function orderInvoicesList(val){
    return {
        type :ORDER_INVOICES,
        valeur:val
    }
}
export function getIdPdf(invoice){
    return {
        type:ID_PDF,
        invoice:invoice
    }
}
export function deleteNewRow(val){
    return{
        type:DELETE_ROW,
        val:val
    }
}
export function changeDateProduct(date,val){
    return{
        type:CHANGE_DATE,
        date:date,
        val:val
    }
}
export function changeInvoiceProducts(name,value,index,products){
    return {
        type:CHANGE_DATA_INVOICE_PRODUCTS,
        name:name,
        value:value,
        index:index,
        products:products
    }
}
export function changeDetailInvProducts(name,value,index,products){
    return {
        type:CHANGE_DETAIL_INVOICE_PRODUCTS,
        name:name,
        value:value,
        index:index,
        products:products
    }
}
export function addRow(){
    return {
        type:ADD_ROW_PRODUCT
    }
}
export function searchInvoicesList(name){
    return {
        type :SEARCH_INVOICES,
        name:name
    }
}
export function detailInvoicesList(invoice){
    return {
        type :DETAIL_INVOICES,
        invoice:invoice
    }
}
export function addInvoiceData(data){
    return{
        type:NEW_INVOICE,
        data:data
    }
}
export function paginationInvoicesList(nbrPage){
    return {
        type :PAGINATION_INVOICE,
        nbrPage:nbrPage
    }
}
export function putInvoice(invoice){
    return{
        type:UPDATE_INVOICE,
        invoice:invoice
    }
}
export function putInvoiceFailed(){
    return{
        type:UPDATE_INVOICE_FAILED
    }
}
export function fetchStarted(){
    return{
        type:FETCH_START
    }
}
export function changeInvoice(name,value){
    return{
        type:CHANGE_VALUE_INVOICE,
        name:name,
        value:value
    }
}
export function dataInvoice(name,value){
    return{
        type:CHANGE_DATA_INVOICE,
        name:name,
        value:value
    }
}
export function changeDateDetailInvProduct(date,val){
    return{
        type:CHANGE_DATE_DETAIL_INVOICE_PRODUCT,
        date:date,
        val:val
    }
}
export function deleteOneInvoice(id){
    return{
        type:DELETE_INVOICE,
        id:id
    }
}
export function addNewInvoice(invoice){
    return{
        type:ADD_INVOICE,
        product:invoice

    }
}