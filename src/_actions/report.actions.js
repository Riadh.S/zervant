import { Service } from '../_services/';
import {FETECHED_REPORT_INVOICE,ORDER_LIST,FILTER_REPORT_INVOICE} from './types';
import { getIdUser } from '../_services/UserServices';
import moment from 'moment';
export const reportActions={
    getSalesReport,
    changeOrder,
    changePeriod
};
function getSalesReport(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'bills/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            let invoices=response.data.data;
            let data=[];
            let mm=moment(invoices[0].billDate).format('MMMM-YYYY');
             let total=0;
             data.push({label:mm,value:total})
             invoices.map((invoice)=>{
             let m=moment(invoice.billDate).format('MMMM-YYYY');
              let test=false;
             data.map((dt)=>{
                if(m===dt.label){
                    dt.value+=invoice.totalTTC;
                    test=true
                }
             });
            if(test===false){
                    data.push({label:m,value:invoice.totalTTC});
            }
            });
            console.log(data);
            dispatch(invoicesReportList(data,invoices));
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function changeOrder(order){
    return dispatch=>{
     dispatch(newOrder(order));
    }
}
function changePeriod(invoices,date1,date2){
    return dispatch=>{
       let d1=moment(date1).format('MM-DD-YYYY');
       let d2=moment(date2).format('MM-DD-YYYY');
        let newInvoices=[];
       invoices.map((invoice)=>{
        let m=moment(invoice.billDate).format('MM-DD-YYYY');
        if((m>=d1)&&(m<=d2)){
            newInvoices.push(invoice);
        }
       });
       let data=[];
       if(newInvoices.length!==0){
       let mm=moment(newInvoices[0].billDate).format('MMMM-YYYY');
        let total=0;
        data.push({label:mm,value:total})
        newInvoices.map((invoice)=>{
        let m=moment(invoice.billDate).format('MMMM-YYYY');
         let test=false;
        data.map((dt)=>{
           if(m===dt.label){
               dt.value+=invoice.totalTTC;
               test=true
           }
        });
       if(test===false){
               data.push({label:m,value:invoice.totalTTC});
       }
       });
    }
       console.log(data);
       dispatch(newInvoicesReportList(data,newInvoices));

    
}
}
export function invoicesReportList(data,invoices){
    return{
        type:FETECHED_REPORT_INVOICE,
        data:data,
        invoices:invoices  
    }
}
export function newOrder(order){
    return{
        type:ORDER_LIST,
        order:order
    }
}
export function newInvoicesReportList(data,newInvoices){
    return{
        type:FILTER_REPORT_INVOICE,
        data:data,
        newInvoices:newInvoices  
    }
}