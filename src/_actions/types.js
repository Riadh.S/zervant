export const LOGIN_SUCCESS = "login_success";
export const LOGIN_FAILED = "login_failed";
export const LOGOUT_SUCCESS = "logout_success";
export const SET_USER = "set_user";
export const CLEAR_USER = "clear_user";
//user actions
export const REGISTER_SUCCESS = "register_success";
export const REGISTER_FAILED = "register_failed";
export const USER_INFORMATION = "information user";
//product actions
export const ALL_PRODUCTS = "fetched_all_data";
export const ORDER_PRODUCTS = "order lists of products";
export const SEARCH_PRODUCTS = "search product";
export const DETAIL_PRODUCTS = "detail product";
export const PAGINATION_PRODUCT = "pagination product";
export const UPDATE_PRODUCT_FAILED = "update product failed";
export const UPDATE_PRODUCT = "update product";
export const CHANGE_VALUE_PRODUCT = "change value product";
export const DELETE_PRODUCT = "delete product";
export const CHANGE_DATA_PRODUCT = "change data product";
export const ADD_PRODUCT = "add product";
export const PRODUCT_START = "PRODUCT_START";
//company information actions
export const COMPANY_INFORMATION_FOUND = "COMPANY_INFORMATION_FOUND";
export const COMPANY_INFORMATION_NOTFOUND = "COMPANY_INFORMATION_NOT_FOUND";
export const CHANGE_DATA_COMPANY = "change data company";
export const ADD_COMPANY = "add new company";
export const UPDATE_COMPANY = "update company";
//payment information actions
export const PAYMENT_INFORMATION_FOUND = "PAYMENT_INFORMATION_FOUND";
export const PAYMENT_INFORMATION_NOTFOUND = "PAYMENT_INFORMATION_NOT_FOUND";
export const CHANGE_DATA_PAYMENT = "change data PAYMENT";
export const ADD_PAYMENT = "add new PAYMENT";
export const UPDATE_PAYMENT = "update PAYMENT";
//BILLSETTING information actions
export const BILLSETTING_INFORMATION_FOUND = "BILLSETTING_INFORMATION_FOUND";
export const BILLSETTING_INFORMATION_NOTFOUND =
  "BILLSETTING_INFORMATION_NOT_FOUND";
export const CHANGE_DATA_BILLSETTING = "change data BILLSETTING";
export const ADD_BILLSETTING = "add new BILLSETTING";
export const UPDATE_BILLSETTING = "update BILLSETTING";
export const CHANGE_CHECKBOX_DATA_BILLSETTING =
  "CHANGE_CHECKBOX_DATA_BILLSETTING";
//DEVISSETTING information actions
export const DEVISSETTING_INFORMATION_FOUND = "DEVISSETTING_INFORMATION_FOUND";
export const DEVISSETTING_INFORMATION_NOTFOUND =
  "DEVISSETTING_INFORMATION_NOT_FOUND";
export const CHANGE_DATA_DEVISSETTING = "change data DEVISSETTING";
export const ADD_DEVISSETTING = "add new DEVISSETTING";
export const UPDATE_DEVISSETTING = "update DEVISSETTING";
export const CHANGE_CHECKBOX_DATA_DEVISSETTING =
  "CHANGE_CHECKBOX_DATA_DEVISSETTING";
//INVOICE actions
export const FETECHED_ALL_INVOICE = "fetched_all_data";
export const ORDER_INVOICES = "order lists of INVOICEs";
export const SEARCH_INVOICES = "search INVOICE";
export const DETAIL_INVOICES = "detail INVOICE";
export const PAGINATION_INVOICE = "pagination INVOICE";
export const UPDATE_INVOICE_FAILED = "update INVOICE failed";
export const UPDATE_INVOICE = "update INVOICE";
export const CHANGE_VALUE_INVOICE = "change value INVOICE";
export const DELETE_INVOICE = "delete INVOICE";
export const CHANGE_DATA_INVOICE = "change data INVOICE";
export const ADD_INVOICE = "add INVOICE";
export const INVOICE_NOT_FOUND = "INVOICE_NOT_FOUND";
export const FETCH_START = "FETCH_START";
export const CHANGE_DATA_INVOICE_PRODUCTS = "CHANGE_DATA_INVOICE_PRODUCTS";
export const ADD_ROW_PRODUCT = "ADD_ROW_PRODUCT";
export const NEW_INVOICE = "NEW_INVOICE";
export const CHANGE_DATE = "CHANGE_DATE";
export const ID_PDF = "ID_PDF";
export const DELETE_ROW = "DELETE_ROW";
export const EDIT_INVOICE = "EDIT_INVOICE";
export const CHANGE_DATE11 = "CHANGE_DATE11";
export const CHANGE_DATE22 = "CHANGE_DATE22";
export const CHANGE_DETAIL_INVOICE = "CHANGE_DETAIL_INVOICE";
export const ADD_ROW_DETAIL_INVOICE = "ADD_ROW_DETAIL_INVOICE";
export const CHANGE_DETAIL_INVOICE_PRODUCTS = "CHANGE_DETAIL_INVOICE_PRODUCTS";
export const DELETE_ROW_DETAIL_INVOICE_PRODUCT =
  "DELETE_ROW_DETAIL_INVOICE_PRODUCT";
export const CHANGE_DATE_DETAIL_INVOICE_PRODUCT =
  "CHANGE_DATE_DETAIL_INVOICE_PRODUCT";
export const PUT_DETAIL_INVOICE = "PUT_DETAIL_INVOICE";
//Estimate actions
export const FETECHED_ESTIMATES = "fetched_all_data";
export const ORDER_ESTIMATES = "order lists of ESTIMATE";
export const SEARCH_ESTIMATES = "search ESTIMATE";
export const DETAIL_ESTIMATES = "detail ESTIMATE";
export const PAGINATION_ESTIMATE = "pagination ESTIMATE";
export const UPDATE_ESTIMATE_FAILED = "update ESTIMATE failed";
export const UPDATE_ESTIMATE = "update ESTIMATE";
export const CHANGE_VALUE_ESTIMATE = "change value ESTIMATE";
export const DELETE_ESTIMATE = "delete ESTIMATE";
export const CHANGE_DATA_ESTIMATE = "change data ESTIMATE";
export const ADD_ESTIMATE = "add ESTIMATE";
export const ESTIMATE_NOT_FOUND = "ESTIMATE_NOT_FOUND";
export const SEARCH_START = "SEARCH_START";
export const CHANGE_DATA_ESTIMATE_PRODUCTS = "CHANGE_DATA_ESTIMATE_PRODUCTS";
export const ADD_ROW_PRODUCTS = "ADD_ROW_PRODUCTS";
export const NEW_ESTIMATE = "NEW_ESTIMATE";
export const CHANGE_DATE_ESTIMATE = "CHANGE_DATE_ESTIMATE";
export const ID_PDF_ESTIMATE = "ID_PDF_ESTIMATE";
export const DELETE_ROW_PRODUCT = "DELETE_ROW_PRODUCT";
export const EDIT_ESTIMATE = "EDIT_ESTIMATE";
export const CHANGE_DETAIL_ESTIMATE = "CHANGE_DETAIL_ESTIMATE";
export const ADD_DETAILPRODUCT_ROW_PRODUCT = "ADD_DETAILPRODUCT_ROW_PRODUCT";
export const CHANGE_DETAIL_ESTIMATE_PRODUCTS =
  "CHANGE_DETAIL_ESTIMATE_PRODUCTS";
export const CHANGE_DATE1 = "CHANGE_DATE1";
export const CHANGE_DATE2 = "CHANGE_DATE2";
export const DELETE_ROW_DETAIL_PRODUCT = "DELETE_ROW_DETAIL_PRODUCT";
export const CHANGE_DATE_DETAIL_ESTIMATE_PRODUCT =
  "CHANGE_DATE_DETAIL_ESTIMATE_PRODUCT";
export const FILTER_LIST_ESTIMATE = "FILTER_LIST_ESTIMATE";
//customer actions
export const FETECHED_ALL_CUSTOMERS = "fetched_all_customers";
export const ORDER_CUSTOMERS = "order lists of customer";
export const SEARCH_CUSTOMERS = "search customer";
export const DETAIL_CUSTOMERS = "detail customer";
export const PAGINATION_CUSTOMER = "pagination customer";
export const UPDATE_CUSTOMER_FAILED = "update customer failed";
export const UPDATE_CUSTOMER = "update customer";
export const CHANGE_VALUE_CUSTOMER = "change value customer";
export const DELETE_CUSTOMER = "delete customer";
export const CHANGE_DATA_CUSTOMER = "change data customer";
export const ADD_CUSTOMER = "add customer";
export const CHANGE_CHECKBOX_DATA_CUSTOMER = "CHANGE_CHECKBOX_DATA_CUSTOMER";
export const ADD_STAR_DELIVERY_METHOD = "DELIVERY METHOD CHOSEN";
export const MODAL_ADD_VISIBLITY_CHANGED = "MODAL_ADD_VISIBLITY_CHANGED";
export const MODAL_EDIT_VISIBLITY_CHANGED = "MODAL_EDIT_VISIBLITY_CHANGED";
export const DEFAULT_CUSTOMER_DATA = "DEFAULT_CUSTOMER_DATA";
//home actions
export const FETECHED_HOME_INVOICE = "FETECHED_HOME_INVOICE";
//report actions
export const FETECHED_REPORT_INVOICE = "FETECHED_REPORT_INVOICE";
export const ORDER_LIST = "ORDER_LIST";
export const FILTER_REPORT_INVOICE = "FILTER_REPORT_INVOICE";
//admin Actions
export const FETECHED_USERS = "FETECHED_USERS";
export const PAGINATION_USER = "PAGINATION_USER";
export const DELETE_USER = "DELETE_USER";
export const LIST_SUPPORT = "LIST_SUPPORT";
export const PAGINATION_SUPPORT = "PAGINATION_SUPPORT";
export const MESSAGE_FROM_SERVER = "MESSAGE_FROM_SERVER";
//support Actions
export const LIST_MESSAGES = "LIST_MESSAGES";
export const CHANGE_DATA_MESSAGE = "CHANGE_DATA_MESSAGE";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const PAGINATION_MESSAGE = "PAGINATION_MESSAGE";
// Locale Actions
export const LOCALE_SET = "LOCALE_SET";
