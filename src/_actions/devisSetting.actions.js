import { DEVISSETTING_INFORMATION_FOUND,DEVISSETTING_INFORMATION_NOTFOUND,ADD_DEVISSETTING,UPDATE_DEVISSETTING,CHANGE_CHECKBOX_DATA_DEVISSETTING,CHANGE_DATA_DEVISSETTING} from "./types.js";
import {Service} from '../_services/Service'
import { getIdUser } from '../_services/UserServices';
import { toast } from 'react-toastify';
export const devisSettingActions={
    getInformationDevisSetting,
    changeDataDevisSetting,
    changeCheckBoxDevisSetting,
    addDevis,
    updateDataDevis
}
function changeCheckBoxDevisSetting(name,value){
   console.log(name+"++++"+value);
    return dispatch=>{
        dispatch(dataDevisCheckBox(name,value));
    }
}
function getInformationDevisSetting(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'devis_setting/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            console.log(response);
            if(response.data.data){
            dispatch(changeInformationDevisSetting(response.data.data));
            }
            else{
                dispatch(noInformationDevisSetting());
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
            dispatch(noInformationDevisSetting());
        })
    };
}
function addDevis(devis){
    console.log(devis);
    return dispatch => {
        let apiEndpoint = 'devis_setting';
        let payload=devis;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            toast.success("your estimate setting is added"); 
            dispatch(addNewDevis(devis));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
function updateDataDevis(payment,id){
    return dispatch => {
        let apiEndpoint = 'devis_setting/'+id;
        let payload=payment;
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            toast.success("your estimate setting is updated"); 
            dispatch(updateDevis(payment));
        }).catch((err)=>{
            toast.error("Error");
            console.log(err);
        })
    };
}
function changeDataDevisSetting(name,value){
    return dispatch=>{
        dispatch(dataDevis(name,value));
    }
}
export function changeInformationDevisSetting(informations){
    return {
        type:DEVISSETTING_INFORMATION_FOUND,
        data:informations
}
}
export function noInformationDevisSetting(){
    return {
        type:DEVISSETTING_INFORMATION_NOTFOUND,
}
}
export function dataDevis(name,value){
    return{
        type:CHANGE_DATA_DEVISSETTING,
        name:name,
        value:value
    }
}
export function dataDevisCheckBox(name,value){
    return{
        type:CHANGE_CHECKBOX_DATA_DEVISSETTING,
        name:name,
        value:value
    }
}
export function addNewDevis(devis){
    return{
        type:ADD_DEVISSETTING,
        devis:devis
    }
}
export function updateDevis(payment){
    return{
        type:UPDATE_DEVISSETTING,
        payment:payment
    }
}