import { Service } from "../_services";
import { getCustomerDefault } from "../config/customerData";
import {
  FETECHED_ALL_CUSTOMERS,
  ADD_CUSTOMER,
  ORDER_CUSTOMERS,
  CHANGE_DATA_CUSTOMER,
  SEARCH_CUSTOMERS,
  DELETE_CUSTOMER,
  DETAIL_CUSTOMERS,
  PAGINATION_CUSTOMER,
  UPDATE_CUSTOMER,
  UPDATE_CUSTOMER_FAILED,
  CHANGE_VALUE_CUSTOMER,
  CHANGE_CHECKBOX_DATA_CUSTOMER,
  ADD_STAR_DELIVERY_METHOD,
  MODAL_ADD_VISIBLITY_CHANGED,
  MODAL_EDIT_VISIBLITY_CHANGED,
  DEFAULT_CUSTOMER_DATA
} from "./types";
import { getIdUser } from "../_services/UserServices";

export const customerActions = {
  getCustomers,
  orderCustomers,
  searchCustomer,
  detailCustomer,
  CustomerPagination,
  updateCustomer,
  changeValueCustomer,
  deleteCustomer,
  changeDataCustomer,
  addCustomer,
  changeCheckBoxCustomer,
  dataCustomerCheckBox,
  addStarDeliveryMethod,
  handleModalVisibilityChanged,
  getCustomerDefaultData
};

const getCustomer = getCustomerDefault();

export function getCustomerDefaultData() {
  return dispatch => {
    dispatch(getDefaultCustomers());
  };
}
export function getDefaultCustomers() {
  return {
    type: DEFAULT_CUSTOMER_DATA,
    payload: { ...getCustomer }
  };
}
export function handleModalVisibilityChanged(visible, modalType) {
  if (modalType === "add") {
    return {
      type: MODAL_ADD_VISIBLITY_CHANGED,
      payload: visible
    };
  } else if (modalType === "edit")
    return {
      type: MODAL_EDIT_VISIBLITY_CHANGED,
      payload: visible
    };
}
export function changeCheckBoxCustomer(name, value) {
  return dispatch => {
    dispatch(dataCustomerCheckBox(name, value));
  };
}
export function dataCustomerCheckBox(name, value) {
  return {
    type: CHANGE_CHECKBOX_DATA_CUSTOMER,
    payload: { name, value }
  };
}
export function addStarDeliveryMethod(value, label) {
  return {
    type: ADD_STAR_DELIVERY_METHOD,

    payload: { value, label }
  };
}
export const getCustomers = () => {
  const _id = getIdUser();
  return dispatch => {
    let apiEndpoint = "customers/" + _id;
    Service.get(apiEndpoint)
      .then(response => {
        console.log("response", response);
        dispatch(changeCustomersList(response.data.data));
      })
      .catch(err => {
        console.log("y'a une Error", err);
      });
  };
};
export const addCustomer = customer => {
  return dispatch => {
    let apiEndpoint = "customers";
    let payload = customer;
    Service.post(apiEndpoint, payload)
      .then(response => {
        dispatch(addNewCustomer(response.data.data));
      })
      .catch(err => {
        console.error("Error add", err);
      });
  };
};
function orderCustomers(val) {
  return dispatch => {
    dispatch(orderCustomersList(val));
  };
}
function changeValueCustomer(name, value) {
  return dispatch => {
    dispatch(changeCustomer(name, value));
  };
}
function changeDataCustomer(name, value) {
  return dispatch => {
    dispatch(dataCustomer(name, value));
  };
}
function searchCustomer(name) {
  return dispatch => {
    dispatch(searchCustomersList(name));
  };
}
function CustomerPagination(nbrPage) {
  console.log(nbrPage);
  return dispatch => {
    dispatch(paginationCustomersList(nbrPage));
  };
}
function detailCustomer(customer) {
  return dispatch => {
    dispatch(detailCustomersList(customer));
  };
}
export const updateCustomer = (_id, customer) => {
  return dispatch => {
    let apiEndpoint = "customers/" + _id;
    let payload = customer;
    console.log(payload);
    Service.put(apiEndpoint, payload)
      .then(response => {
        if (response.status === 200) {
          dispatch(putCustomer(customer));
        }
      })
      .catch(err => {
        console.log("Error");
        console.log(err);
        dispatch(putCustomerFailed());
      });
  };
};
export const deleteCustomer = id => {
  return dispatch => {
    let apiEndpoint = "customers/" + id;
    Service.deleteDetail(apiEndpoint)
      .then(res => {
        if (res.status === 200) {
          dispatch(deleteOneCustomer(id));
        }
      })
      .catch(err => console.log(err));
  };
};
const changeCustomersList = customers => {
  return {
    type: FETECHED_ALL_CUSTOMERS,
    payload: customers
  };
};
export function orderCustomersList(val) {
  return {
    type: ORDER_CUSTOMERS,
    payload: val
  };
}
export function searchCustomersList(name) {
  return {
    type: SEARCH_CUSTOMERS,
    payload: name
  };
}
export function detailCustomersList(customer) {
  return {
    type: DETAIL_CUSTOMERS,
    payload: customer
  };
}
export function paginationCustomersList(nbrPage) {
  return {
    type: PAGINATION_CUSTOMER,
    payload: nbrPage
  };
}
const putCustomer = customer => {
  return {
    type: UPDATE_CUSTOMER,
    payload: customer
  };
};
const putCustomerFailed = () => {
  return {
    type: UPDATE_CUSTOMER_FAILED
  };
};
export function changeCustomer(name, value) {
  return {
    type: CHANGE_VALUE_CUSTOMER,

    payload: { name, value }
  };
}
export function dataCustomer(name, value) {
  console.log("dataCustomer changed", name, " : ", value);
  return {
    type: CHANGE_DATA_CUSTOMER,
    payload: { name, value }
  };
}
const deleteOneCustomer = id => {
  return {
    type: DELETE_CUSTOMER,
    payload: id
  };
};
const addNewCustomer = customer => {
  return {
    type: ADD_CUSTOMER,
    payload: customer
  };
};
