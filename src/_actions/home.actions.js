import { Service } from '../_services/';
import {FETECHED_HOME_INVOICE} from './types';
import { getIdUser } from '../_services/UserServices';
import { saveAs } from 'file-saver';
export const homeActions={
    updateData,
getDataInvoices
};
async function updateData(){
    const id=getIdUser();
    let apiEndpoint = 'bills/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            let invoices=response.data.data;
        invoices.map((invoice)=>{
          let date=invoice.deadline;
          date=new Date(date);
          let dateNow=new Date();
          if((date<dateNow)&&(invoice.status!=="Paid")){
              let idInv=invoice._id;
              let apiEndpoint='bills/'+idInv;
              let payload={status:"Overdue"};
              Service.put(apiEndpoint,payload)
              .then((response)=>{
               console.log(response.data.data);             
              }
          
        )
        .catch((err)=>console.log(err));
            }
        });
});
}
function getDataInvoices(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'bills/'+id;
        Service.get(apiEndpoint)
        .then((response)=>{
            let invoices=response.data.data;
            let created=0;
            let paid=0;
            let overdue=0;
            let data=[];
             invoices.map((invoice)=>{
              if(invoice.status==="created"){
                  created=created+invoice.totalTTC;
              }
              if(invoice.status==="Paid"){
                paid=paid+invoice.totalTTC;
            }
            if(invoice.status==="Overdue"){
                overdue=overdue+invoice.totalTTC;
            }
            });
        data.push(paid,created,overdue);
        console.log(data);
        console.log(paid);
            dispatch(invoicesHomeList(data,created,paid,overdue));
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
export function invoicesHomeList(data,created,paid,overdue){
    return{
        type:FETECHED_HOME_INVOICE,
        data:data,
        created:created,
        paid:paid,
        overdue:overdue
    }
}