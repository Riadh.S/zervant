import { Service } from "../_services";
import { history } from "../_helpers";
import {
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  LOGIN_FAILED,
  SET_USER,
  CLEAR_USER
} from "./types.js";

export const authActions = {
  login,
  logout
};

export const setUser = (token, auth) => {
  return {
    type: SET_USER,
    payload: {
      token,
      auth
    }
  };
};

export const clearUser = () => {
  return {
    type: CLEAR_USER
  };
};

export const login = (email, password) => {
  return dispatch => {
    let apiEndpoint = "auths";
    let payload = {
      email: email,
      password: password
    };
    Service.post(apiEndpoint, payload)
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          localStorage.setItem("token", response.data.token);
          localStorage.setItem("auth", response.data.auth);
          dispatch(setUserDetails(response.data));
          history.replace("/home");
        }
        if (response.status === 201) {
          dispatch(loginFailed(response.data.message));
        }
        if (response.status === 202) {
          dispatch(loginFailed(response.data.message));
        }
      })
      .catch(err => {
        dispatch(loginFailed("problem"));
        console.log(err);
      });
  };
};
export const logout = () => {
  return dispatch => {
    localStorage.removeItem("auth");
    localStorage.removeItem("token");
    dispatch(logoutUser());
    history.replace("/login");
  };
};

const setUserDetails = user => {
  return {
    type: LOGIN_SUCCESS,
    auth: user.auth,
    token: user.token,
    userResponse: ""
  };
};

const logoutUser = () => {
  return {
    type: LOGOUT_SUCCESS,
    auth: false,
    token: ""
  };
};
const loginFailed = err => {
  return {
    type: LOGIN_FAILED,
    auth: false,
    token: "",
    userResponse: err
  };
};
