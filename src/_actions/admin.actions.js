import { Service } from '../_services/';
import {FETECHED_USERS,PAGINATION_USER,PAGINATION_SUPPORT,DELETE_USER,LIST_SUPPORT,MESSAGE_FROM_SERVER} from './types';
import { getIdUser } from '../_services/UserServices';
export const adminActions={
    getUsers,
    adminPagination,
    deleteUser,
    getSupports,
    supportPagination,
    messageAdded
};
function getUsers(){
    return dispatch => {
        let apiEndpoint = 'users';
        Service.get(apiEndpoint)
        .then((response)=>{
            dispatch(usersList(response.data.data));
            
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function messageAdded(message){
    return dispatch=>{
        dispatch(messageFromServer(message));
    }
}
function getSupports(){
    return dispatch => {
        let apiEndpoint = 'support';
        Service.get(apiEndpoint)
        .then((response)=>{
            dispatch(supportList(response.data.data));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function deleteUser(user){
    let id=user._id;
    return dispatch => {
        let apiEndpoint = 'users/'+id;
        Service.deleteDetail(apiEndpoint)
        .then((response)=>{
            dispatch(deleteOneUser(user));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    };
}
function adminPagination(nbrPage){
    return dispatch=>{
        dispatch(userPagination(nbrPage));
    }
}
function supportPagination(nbrPage){
    return dispatch=>{
        dispatch(suppPagination(nbrPage));
    }
}
export function messageFromServer(message){
    return{
        type:MESSAGE_FROM_SERVER,
        message:message
    }
}
export function supportList(support){
    return{
        type:LIST_SUPPORT,
        support:support
    }
}
export function suppPagination(nbrPage){
    return{
        type:PAGINATION_SUPPORT,
        nbrPage:nbrPage
    }
}
export function deleteOneUser(user){
    return{
        type:DELETE_USER,
        user:user
    }
}
export function usersList(data){
    return{
        type:FETECHED_USERS,
        data:data
    }
}
export function userPagination(nbrPage){
    return{
        type:PAGINATION_USER,
        nbrPage:nbrPage
    }
}
