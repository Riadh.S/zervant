import { Service } from '../_services/';
import {FETECHED_ESTIMATES,ID_PDF_ESTIMATE,FILTER_LIST_ESTIMATE,ADD_DETAILPRODUCT_ROW_PRODUCT,CHANGE_DATE_DETAIL_ESTIMATE_PRODUCT,DELETE_ROW_DETAIL_PRODUCT,CHANGE_DATE11,CHANGE_DATE22,CHANGE_DETAIL_ESTIMATE_PRODUCTS,CHANGE_DETAIL_ESTIMATE,EDIT_ESTIMATE,CHANGE_DATE_ESTIMATE,DELETE_ROW_PRODUCT,ADD_ESTIMATE,ADD_ROW_PRODUCTS,NEW_ESTIMATE,CHANGE_DATA_ESTIMATE_PRODUCTS,SEARCH_START,ORDER_ESTIMATES,CHANGE_DATA_ESTIMATE,SEARCH_ESTIMATES,DELETE_ESTIMATE,DETAIL_ESTIMATES,PAGINATION_ESTIMATE,UPDATE_ESTIMATE,UPDATE_ESTIMATE_FAILED,CHANGE_VALUE_ESTIMATE, CHANGE_DATA_DEVISSETTING} from './types';
import { getIdUser } from '../_services/UserServices';
import { saveAs } from 'file-saver';
export const estimateActions = {
    getEstimates,
    orderEstimates,
    searchEstimate,
    detailEstimate,
    estimatePagination,
    updateEstimate,
    changeValueEstimate,
    deleteEstimate,
    changeDataEstimate,
    addEstimate,
    generatePdf,
    getPDF,
    changeDataEstimateProducts,
    addRowProducts,
    newEstimate,
    changeDateProduct,
    deleteRow,
    editDataEstimate,
    sendEmail,
    changeDetailEstimate,
    addDetailEstimateRowProducts,
    changeDetailEstimateProducts,
    changeDate11,
    changeDate22,
    deleteRowFromDetailEstimate,
    changeDateDetailEstimateProduct,
    ConvertToInvoice
};

function getEstimates(){
    const id=getIdUser();
    return dispatch => {
        let apiEndpoint = 'devis/'+id;
        dispatch(fetchStart());
        Service.get(apiEndpoint)
        .then((response)=>{
            let estimates=response.data.data;
            let apiEndpoint = 'products/'+id;
            Service.get(apiEndpoint)
             .then((response)=>{
                let products=response.data.data;
                let apiEndpoint = 'customers/'+id;
                Service.get(apiEndpoint)
                .then((response)=>{
                    let customers=response.data.data;
                    console.log(response.data.data); 
                   dispatch(estimatesList(estimates,products,customers));
             }) 
        })
    })
        .catch((err)=>{
            console.log("Error");
            console.log(err);
        })
}
}
function deleteRow(val){
    console.log("++++++"+val);
    return dispatch=>{
        dispatch(deleteNewRow(val));
    }
}
function deleteRowFromDetailEstimate(val){
    return dispatch=>{
        dispatch(deleteRowDetailProduct(val));
    }
}
function changeDate11(date){
    return dispatch=>{
        dispatch(updateDate11(date));
    }
}
function changeDate22(date){
    return dispatch=>{
        dispatch(updateDate22(date));
    }
}
function sendEmail(estimate){
    
        let apiEndpoint = 'devis/send-email';
        let payload=estimate;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            console.log(response.data.message);
            //dispatch(sendEmailSuccess(response.data.message));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })

}
function newEstimate(data){
    console.log(data);
    return dispatch=>{
        dispatch(addEstimateData(data));
    }
}
function changeDateProduct(date,val){
    return dispatch=>{
        dispatch(changeDateProducts(date,val));
    }
}
function changeDateDetailEstimateProduct(date,val){
    return dispatch=>{
        dispatch(changeDateDetailEstiProduct(date,val));
    }
}
function addDetailEstimateRowProducts(){
    return dispatch=>{
    dispatch(addDetailEstiRowProducts());
    }
}
function editDataEstimate(){
    return dispatch=>{
       dispatch(editEsti());
    }
}
function changeDetailEstimate(name,value){
    return dispatch=>{
        dispatch(changeDetailEsti(name,value));
    }
}
function addRowProducts(){
    return dispatch=>{
     dispatch(addRows());
}
}
function generatePdf(estimate){
    return dispatch=>{
    let apiEndpoint = 'devis/create-pdf';
        let payload=estimate;
        Service.post(apiEndpoint,payload)

        .then((res) =>{
        if(res.data.data){
            console.log(res.data.data);
          dispatch(getIdPdf(res.data.data)); 
        }
    })
   .catch((err)=>{
    console.log(err);
   });
}
}
function ConvertToInvoice(estimate,id){
    return dispatch=>{
        let apiEndpoint = 'devis/convert-estimate/'+id;
            let payload=estimate;
            Service.post(apiEndpoint,payload)
            .then((res) =>{
            if(res.data.data){
                console.log(res.data.data);
              dispatch(filterEstimate()); 
            }
        })
       .catch((err)=>{
        console.log(err);
       });
    } 
}
function getPDF(){
    let apiEndpoint = 'devis/fetch-pdf';
    Service.getpdf(apiEndpoint)
    .then((res) => { 
        
        const pdfBlob = new Blob([res.data], { type: 'application/pdf' });
        console.log(pdfBlob);
        saveAs(pdfBlob, 'invoice.pdf');
    }
      
)
.catch((err)=>console.log(err));
}
function addEstimate(invoice){
    return dispatch => {
        let apiEndpoint = 'devis';
        let payload=invoice;
        Service.post(apiEndpoint,payload)
        .then((response)=>{
            dispatch(addNewEstimate(invoice));
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    }
}
function changeDataEstimateProducts(name,value,index,products){
    console.log(products);
    return dispatch=>{
        dispatch(changeEstimateProducts(name,value,index,products));
    }
}
function changeDetailEstimateProducts(name,value,index,products){
    console.log(products);
    return dispatch=>{
        dispatch(changeDetailEstiProducts(name,value,index,products));
    }
}
function orderEstimates(val){

    return dispatch=>{
        dispatch(orderEstimatesList(val));
    }
}
function changeValueEstimate(name,value){
    return dispatch=>{
        dispatch(changeEstimate(name,value));
    }
}
function changeDataEstimate(name,value){
    return dispatch=>{
        dispatch(dataEstimate(name,value));
    }
}
function searchEstimate(estimateName){
    return dispatch=>{
        dispatch(searchEstimatesList(estimateName));
    }
}
function estimatePagination(nbrPage){
    console.log(nbrPage);
    return dispatch=>{
        dispatch(paginationEstimatesList(nbrPage));
    }
}
function detailEstimate(estimate){
    return dispatch=>{
        dispatch(detailEstimatesList(estimate));
    }
}
function updateEstimate(estimate,id){
    return dispatch=>{    
    let apiEndpoint = 'devis/update-pdf/'+id;
        let payload =estimate;
        console.log(payload);
        Service.put(apiEndpoint,payload)
        .then((response)=>{
            if(response.status===200){
            console.log(response.data.data);
            dispatch(putEstimate(response.data.data));
            }
        }).catch((err)=>{
            console.log("Error");
            console.log(err);
        })
    
}
}
function deleteEstimate(id){
    return dispatch =>{
        let apiEndpoint = 'devis/'+id;
        Service.deleteDetail(apiEndpoint)
        .then((res)=>{
            if(res.status===200){
                dispatch(deleteOneEstimate(id));
            }
        })
        .catch((err)=>console.log(err))
    }
}
export function estimatesList(estimates,products,customers){
    console.log(estimates);
    return{
        type: FETECHED_ESTIMATES,
        estimates: estimates,
        products:products,
        customers:customers
    }
}
export function deleteRowDetailProduct(val){
    return{
        type:DELETE_ROW_DETAIL_PRODUCT,
        val:val
    }
}
/*export function noChangeInvoicesList(){
    return{
        type:INVOICE_NOT_FOUND
    }
}*/
export function updateDate11(date){
   return{
       type:CHANGE_DATE11,
       date:date
   }
}
export function updateDate22(date){
    return{
        type:CHANGE_DATE22,
        date:date
    }
}
export function addDetailEstiRowProducts(){
    return {
        type:ADD_DETAILPRODUCT_ROW_PRODUCT
    }
}
export function changeDetailEsti(name,value){
    return{
        type:CHANGE_DETAIL_ESTIMATE,
        name:name,
        value:value
    }
}
export function orderEstimatesList(val){
    return {
        type :ORDER_ESTIMATES,
        valeur:val
    }
}
export function getIdPdf(estimate){
    return {
        type:ID_PDF_ESTIMATE,
        estimate:estimate
    }
}
export function deleteNewRow(val){
    return{
        type:DELETE_ROW_PRODUCT,
        val:val
    }
}
export function editEsti(){
    return{
        type:EDIT_ESTIMATE
    }
}
export function changeDateProducts(date,val){
    return{
        type:CHANGE_DATE_ESTIMATE,
        date:date,
        val:val
    }
}
export function filterEstimate(){
    return{
        type:FILTER_LIST_ESTIMATE
    }
}
export function changeEstimateProducts(name,value,index,products){
    return {
        type:CHANGE_DATA_ESTIMATE_PRODUCTS,
        name:name,
        value:value,
        index:index,
        products:products
    }
}
export function changeDetailEstiProducts(name,value,index,products){
    return {
        type:CHANGE_DETAIL_ESTIMATE_PRODUCTS,
        name:name,
        value:value,
        index:index,
        products:products
    }
}
export function addRows(){
    return {
        type:ADD_ROW_PRODUCTS
    }
}
export function searchEstimatesList(name){
    return {
        type :SEARCH_ESTIMATES,
        name:name
    }
}
export function changeDateDetailEstiProduct(date,val){
    return{
        type:CHANGE_DATE_DETAIL_ESTIMATE_PRODUCT,
        date:date,
        val:val
    }
}
export function detailEstimatesList(estimate){
    return {
        type :DETAIL_ESTIMATES,
        estimate:estimate
    }
}
export function addEstimateData(data){
    return{
        type:NEW_ESTIMATE,
        data:data
    }
}
export function paginationEstimatesList(nbrPage){
    return {
        type :PAGINATION_ESTIMATE,
        nbrPage:nbrPage
    }
}
export function putEstimate(estimate){
    return{
        type:UPDATE_ESTIMATE,
        estimate:estimate
    }
}
export function putEstimateFailed(){
    return{
        type:UPDATE_ESTIMATE_FAILED
    }
}
export function fetchStart(){
    return{
        type:SEARCH_START
    }
}
export function changeEstimate(name,value){
    return{
        type:CHANGE_VALUE_ESTIMATE,
        name:name,
        value:value
    }
}
export function dataEstimate(name,value){
    return{
        type:CHANGE_DATA_ESTIMATE,
        name:name,
        value:value
    }
}
export function deleteOneEstimate(id){
    return{
        type:DELETE_ESTIMATE,
        id:id
    }
}
export function addNewEstimate(invoice){
    return{
        type:ADD_ESTIMATE,
        product:invoice

    }
}