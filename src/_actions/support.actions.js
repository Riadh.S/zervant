import {LIST_MESSAGES,PAGINATION_MESSAGE,CHANGE_DATA_MESSAGE,ADD_MESSAGE} from './types';
import { getIdUser } from '../_services/UserServices';
import { Service } from '../_services/';
export const supportActions={
    getMessagesSupport,
    changeDataMessage,
    addMessage,
    paginationMessage  
  }
function getMessagesSupport(){
    const id = getIdUser();
    return dispatch => {
      let apiEndpoint = "support/" + id;
      Service.get(apiEndpoint)
        .then(response => {
          if (response.data.data) {
            dispatch(messagesList(response.data.data));
            
          }
        })
        .catch(err => {
          console.log("warning");
          console.log(err);
        });
    };
}
function paginationMessage(nbrPage){
  return dispatch=>{
    dispatch(paginationSupport(nbrPage));
  }
}
function addMessage(message){
    return dispatch => {
        let apiEndpoint = "support";
        let payload=message;
        Service.post(apiEndpoint,payload)
          .then(response => {
            if (response.data.data) {
              dispatch(AddMessageToList(response.data.data));
              
            }
          })
          .catch(err => {
            console.log("warning");
            console.log(err);
          });
      };
}
function changeDataMessage(name,value){
    return dispatch=>{
        dispatch(onChangeMessage(name,value));
    }
}
export function paginationSupport(nbrPage){
  return{
    type:PAGINATION_MESSAGE,
    nbrPage:nbrPage
  }
}
export function AddMessageToList(message){
    return{
        type:ADD_MESSAGE,
        message:message
    }
}
export function onChangeMessage(name,value){
    return{
        type:CHANGE_DATA_MESSAGE,
        name:name,
        value:value
    }
}
export function messagesList(messages){
    return{
        type:LIST_MESSAGES,
        messages:messages
    }
} 