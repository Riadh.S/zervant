import { combineReducers } from "redux";
import { authentication } from "./auth.reducer";
import { users } from "./user.reducer";
import { product } from "./product.reducer";
import { companyInformation } from "./companyInformation.reducer";
import { payment } from "./payment.reducer";
import { userAccount } from "./userAccount.reducer";
import { billSetting } from "./billSetting.reducer";
import { devisSetting } from "./devisSetting.reducer";
import { invoice } from "./invoice.reducer";
import { estimate } from "./estimate.reducer";
import { home } from "./home.reducer";
import locale from "./locale";
import CustomerReducer from "./CustomerReducer";
import { report } from "./report.reducer";
import { admin } from "./admin.reducer";
import { support } from "./support.reducer";
const rootReducer = combineReducers({
  authentication,
  users,
  product,
  companyInformation,
  payment,
  userAccount,
  billSetting,
  devisSetting,
  invoice,
  estimate,
  home,
  report,
  admin,
  support,
  customer: CustomerReducer,
  locale: locale
});
export default rootReducer;
