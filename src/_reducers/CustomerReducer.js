import { getCustomerDefault } from "../config/customerData";
import _ from "lodash";
import {
  FETECHED_ALL_CUSTOMERS,
  ADD_CUSTOMER,
  ORDER_CUSTOMERS,
  CHANGE_DATA_CUSTOMER,
  SEARCH_CUSTOMERS,
  DELETE_CUSTOMER,
  DETAIL_CUSTOMERS,
  PAGINATION_CUSTOMER,
  UPDATE_CUSTOMER,
  UPDATE_CUSTOMER_FAILED,
  CHANGE_VALUE_CUSTOMER,
  CHANGE_CHECKBOX_DATA_CUSTOMER,
  ADD_STAR_DELIVERY_METHOD,
  MODAL_ADD_VISIBLITY_CHANGED,
  MODAL_EDIT_VISIBLITY_CHANGED,
  DEFAULT_CUSTOMER_DATA
} from "../_actions/types";

console.log("getCustomerDefault()", getCustomerDefault());
// const {
//   TypeOfCustomer,
//   country,
//   invoicingLanguage,
//   paymentTerm,
//   eInvoiceOperator,
//   eInvoiceNetworkType
// } = getCustomerDefault();

const INITIAL_STATE = {
  customersList: [],
  searchData: "",
  detailCustomer: "",
  listPage: 4,
  currentPage: 1,
  data: {
    typeOfCustomer: "",
    CompanyName: "",
    CompanyRegistrationNumber: "",
    VatNumber: "",
    title: "",
    firstName: "",
    lastName: "",
    emailCustomer: "",
    phone: "",
    mobilePhone: "",
    address: "",
    postCode: "",
    city: "",
    country: "",
    invoicingLanguage: "",
    paymentTerm: "",
    ByEmail: false,
    ByPost: false,
    None: false
  },
  customer: "",
  customers: [],
  addCustomerModal: false,
  editCustomerModal: false
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETECHED_ALL_CUSTOMERS:
      return {
        ...state,
        customersList: action.payload
      };
    case DEFAULT_CUSTOMER_DATA:
      console.warn("DEFAULT_CUSTOMER_DATA", action.payload);
      return {
        ...state,
        data: {
          ...state.data,
          typeOfCustomer: action.payload.typeOfCustomer,
          invoicingLanguage: action.payload.invoicingLanguage,
          paymentTerm: action.payload.paymentTerm
        }
      };

    case ORDER_CUSTOMERS: {
      const customers = _.orderBy(state.customersList, action.valeur, "asc");
      console.log(customers);
      return {
        ...state,
        customers: action.payload
      };
    }
    case SEARCH_CUSTOMERS: {
      return {
        ...state,
        searchData: action.payload,
        currentPage: 1
      };
    }
    case DETAIL_CUSTOMERS: {
      return {
        ...state,
        detailCustomer: action.payload
      };
    }
    case PAGINATION_CUSTOMER: {
      console.log(action.nbrPage);
      return {
        ...state,
        currentPage: action.payload
      };
    }
    case UPDATE_CUSTOMER_FAILED: {
      return {
        ...state,
        customer: "update customer failed"
      };
    }
    case CHANGE_DATA_CUSTOMER: {
      return {
        ...state,
        data: { ...state.data, [action.payload.name]: action.payload.value }
      };
    }
    case UPDATE_CUSTOMER: {
      // state.customersList.push(action.payload);
      // const newCustomers = [...state.customersList, action.payload];
      // //const newCustomers = { ...state.customersList };
      // const index = newCustomers.indexOf(state.detailCustomer);
      // console.log(index);
      // const customer = action.payload;
      // newCustomers[index] = { ...customer };
      //      for (_id in {customersList})
      //      {
      // if (_id===
      //      }
      /* console.log(newCustomers());
      const customer = { ...action.payload };
      console.log("action.payload", action.payload);
      const customersList = [...state.customersList];
      console.log("customersList", customersList);
      const index = customersList.indexOf(action.payload);
      console.log("index", index);*/
      const customerList = [...state.customersList];
      console.log("customerList", customerList);
      const customer = customerList.find(c => c._id === action.payload._id);
      console.log("customer", customer);
      const index = customerList.indexOf(customer);
      console.log("index", index);
      customerList[index] = action.payload;
      console.log("customerList[index]", customerList[index]);

      // const index =
      return {
        ...state,
        // customer: "",
        detailCustomer: action.payload,
        customersList: customerList,
        editCustomerModal: false
      };
    }

    case ADD_CUSTOMER: {
      //state.customers.push(action.payload);

      // const newCustomers = [...state.customers, action.payload];
      // console.log("newCustomers", newCustomers);
      // const customerData = [...getCustomerDefault()];
      // console.log("customerData", customerData);

      return {
        ...state,
        customersList: [...state.customersList, action.payload],
        addCustomerModal: false
      };
    }
    case CHANGE_VALUE_CUSTOMER: {
      const customer = state.detailCustomer;
      customer[action.payload] = action.value;

      return {
        ...state,
        detailCustomer: action.payload
      };
    }
    case DELETE_CUSTOMER: {
      const newCustomers = state.customers.filter(
        customer => customer._id !== action._id
      );
      console.log(newCustomers);
      return {
        ...state,
        customers: newCustomers
      };
    }

    case CHANGE_CHECKBOX_DATA_CUSTOMER: {
      //console.log(action.name, action.value, "checkbox customer reducer");
      const customer = state.data;

      customer[action.payload.name] = !action.payload.value;
      return {
        ...state,
        data: {
          ...state.data,
          ByEmail: false,
          ByPost: false,

          None: false,
          [action.payload.name]: true
        }
      };
    }
    case ADD_STAR_DELIVERY_METHOD: {
      const customer = state.data;

      customer[action.payload.value] = action.label + "*";

      return {
        ...state,
        data: action.payload
      };
    }

    case MODAL_ADD_VISIBLITY_CHANGED: {
      return {
        ...state,
        addCustomerModal: action.payload,
        editCustomerModal: false,
        data: state.data
      };
    }

    case MODAL_EDIT_VISIBLITY_CHANGED: {
      return {
        ...state,
        editCustomerModal: action.payload,
        addCustomerModal: false,
        data: state.detailCustomer
      };
    }

    default:
      return state;
  }
};
