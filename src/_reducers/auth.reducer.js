import {
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  LOGIN_FAILED,
  SET_USER,
  CLEAR_USER
} from "../_actions/types";
const initialState = {
  token: "",
  auth: "",
  userResponse: "",
  loggingIn: false,
  isLoading: true
};
export function authentication(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        loggingIn: true,
        auth: action.auth,
        token: action.token,
        userResponse: action.userResponse,
        isisLoading: false
      };
    case LOGOUT_SUCCESS:
      return {
        isLoading: false,
        auth: false,
        token: ""
      };
    case LOGIN_FAILED:
      return {
        loggingIn: false,
        auth: false,
        userResponse: action.userResponse
      };
    case SET_USER:
      return {
        isLoading: false,
        auth: action.auth,
        token: action.token
      };
    case CLEAR_USER:
      return {
        isLoading: false,
        auth: false,
        token: ""
      };
    default:
      return state;
  }
}
