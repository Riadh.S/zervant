import {COMPANY_INFORMATION_FOUND,COMPANY_INFORMATION_NOTFOUND,ADD_COMPANY, UPDATE_COMPANY,CHANGE_DATA_COMPANY} from '../_actions/types';
const initialeState={
    dataCompany:{
        siretNumber:'',
        vatNumber:'' ,
        vatDeclaration:'',
        defaultVat:'',
        adress:'',
        codePostal:'',
        city:'',
        webSite:'',
        logo:''    

    },
    found:false
}
export function companyInformation(state=initialeState,action){
    switch (action.type){
        case  COMPANY_INFORMATION_FOUND:{
            return{
                dataCompany:action.data,
               found:true 
            }
        }
        case COMPANY_INFORMATION_NOTFOUND:{
            return{
                dataCompany:{
                    siretNumber:'',
                    vatNumber:'' ,
                    vatDeclaration:'',
                    defaultVat:'',
                    adress:'',
                    codePostal:'',
                    city:'',
                    webSite:'',
                    logo:''    
            
                },
                found:false 
            }
        }
    
    case CHANGE_DATA_COMPANY:{
        const company=state.dataCompany;
        company[action.name]=action.value;
        return{
          ...state,
          dataCompany:company
        }
      }
      case ADD_COMPANY:{
          return {
              ...state,
              found:true
      }
    }
    case UPDATE_COMPANY:{
   return state;
    }
    default:{
      return state;
}
}
}