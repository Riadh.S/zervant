import { DEVISSETTING_INFORMATION_FOUND,DEVISSETTING_INFORMATION_NOTFOUND,ADD_DEVISSETTING,UPDATE_DEVISSETTING,CHANGE_DATA_DEVISSETTING,CHANGE_CHECKBOX_DATA_DEVISSETTING} from "../_actions/types";
const initialState={
    data:{
        currency:'',
        language:'' ,
        devisHeader:'',
        dayValidity:false,
        attentionOf:false,
        displayColunms:[
          {label:'name',value : true},
         { label:'description',value : true},
          {label:'unit',value : true},
          { label:'priceBasedOn',value : true},
           {label:'priceWT',value : true},
          {label:'vat',value : true},
          {label:'priceTTC',value : true}
         ],
        discountSetting:[
          {label:'Total',value  : true},
          {label:'withRow' ,value : true},
          {label:'price' ,value : true},
          {label:'percent' ,value : true},
          {label:'BaseHT' ,value : true},
         {label:'BaseTTC' ,value : true},
        ]  , 
        message:'',
        notesFooter:'',
        ConditionsSale:{valueCS:'',displayCS:true}    
    },
    found:false
}
export function devisSetting(state=initialState,action){
    switch (action.type){
        case  DEVISSETTING_INFORMATION_FOUND:{
          console.log(action.data);  
          return{
               data:action.data,
               found:true 
            }
        }
        case DEVISSETTING_INFORMATION_NOTFOUND:{
            return{
                data:{
                    currency:'',
                    language:'' ,
                    devisHeader:'',
                    dayValidity:false,
                    attentionOf:false,
                    displayColunms:[
                      {label:'name',value : true},
                     { label:'description',value : true},
                      {label:'unit',value : true},
                      { label:'priceBasedOn',value : true},
                       {label:'priceWT',value : true},
                      {label:'vat',value : true},
                      {label:'priceTTC',value : true}
                     ],
                    discountSetting:[
                      {label:'Total',value  : true},
                      {label:'withRow' ,value : true},
                      {label:'price' ,value : true},
                      {label:'percent' ,value : true},
                      {label:'BaseHT' ,value : true},
                     {label:'BaseTTC' ,value : true},
                    ]  , 
                    message:'',
                    notesFooter:'',
                    ConditionsSale:{valueCS:'',displayCS:true}    
                },
                found:false
            }
        }
    
    case CHANGE_DATA_DEVISSETTING:{
        const bill=state.data;
        if(action.name==="valueCS"){
        bill.ConditionsSale[action.name]=action.value;
        }
        else{
        bill[action.name]=action.value;
        }
        return{
          ...state,
          data:bill
        }
      }
      case CHANGE_CHECKBOX_DATA_DEVISSETTING:{
        console.log(action.name,action.value);
        const bill=state.data;
       let info=bill;
        if(action.name==="displayCS"){
        info=bill.ConditionsSale;
       }
        if(action.value===true){
        info[action.name]=false;
      }else{
        info[action.name]=true;
      }
      
      return{
          ...state,
          data:bill
      }
      }
      case ADD_DEVISSETTING:{
          return {
              ...state,
              found:true
      }
    }
    case UPDATE_DEVISSETTING:{
   return state;
    }
    default:{
      return state;
}
}

}