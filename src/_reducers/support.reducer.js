import {LIST_MESSAGES,CHANGE_DATA_MESSAGE,ADD_MESSAGE,PAGINATION_MESSAGE} from '../_actions/types';
let initialeState={
    messages:[],
    searchMessage:'',
    load:false,
    detailMessage:'',
    listPageMessage:4,
    currentMessage:1,
    dataMessage:{
        subject:'',
        message_user:[
            {
                message:'',
                date:'',
                attachement:'',
                status:''
            }
        ]
    }
}
export function support(state=initialeState,action){
switch(action.type){
    case LIST_MESSAGES:{
        console.log(action.messages);
        return{
            ...state,
           messages:action.messages
        }
    }
    case CHANGE_DATA_MESSAGE:{
        let msg=state.dataMessage;
        if(action.name==="subject"){
            msg[action.name]=action.value;
        }
        if(action.name==="message"){
            msg.message_user[0].message=action.value;
        }
        return{
            ...state,
            dataMessage:msg
        } 
    }
    case PAGINATION_MESSAGE:{
        return {
            ...state,
            currentMessage:action.nbrPage
        }
    }
    case ADD_MESSAGE:{
       let messages=state.messages;
       messages.push(action.message);
       return{
          ...state,
          messages:messages,
          dataMessage:{
            subject:'',
            message_user:[
                {
                    message:'',
                    date:'',
                    attachement:'',
                    status:''
                }
            ]   
          } 
       } 
    }
    default:{
        return state;
    }
}
}