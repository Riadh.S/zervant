import {FETECHED_ESTIMATES,FILTER_LIST_ESTIMATE,CHANGE_DETAIL_ESTIMATE_PRODUCTS,CHANGE_DATE_DETAIL_ESTIMATE_PRODUCT,DELETE_ROW_DETAIL_PRODUCT,CHANGE_DATE1,CHANGE_DATE2,ADD_DETAILPRODUCT_ROW_PRODUCT,CHANGE_DETAIL_ESTIMATE,CHANGE_DATE_ESTIMATE,EDIT_ESTIMATE,DELETE_ROW_PRODUCT,ADD_ESTIMATE,ADD_ROW_PRODUCTS,NEW_ESTIMATE,CHANGE_DATA_ESTIMATE_PRODUCTS,SEARCH_START,ORDER_ESTIMATES,CHANGE_DATA_ESTIMATE,SEARCH_ESTIMATES,DELETE_ESTIMATE,DETAIL_ESTIMATES,PAGINATION_ESTIMATE,UPDATE_ESTIMATE,UPDATE_ESTIMATE_FAILED,CHANGE_VALUE_ESTIMATE, ID_PDF_ESTIMATE} from '../_actions/types';
import _ from 'lodash';

const initialState={
  searchEstimate:'',
        detailEstimate:'',
        listEstimate:4,
        currentPageEstimate:1,
        dataPdfEstimate:'', 
        length:0,
        table:[],
        startDate1:new Date(),
        startDate2:new Date(), 
        numberEstimate:0,
        dataProductsEstimate:[],
        dataCustomersEstimate:[],    
     dataEstimate:{
        companyName: '',
        billNumber : 0,
        billDate : '',
        conditionPayment : 0,
        deadline :'',
        message : '',
        notesFooter : '',
        net:0,  
        discount : 0,
        totalTTC : 0,
        products:[
          {
            id_product : '',
             name : '',
             date :new Date(),
            quatity : 1,
            unit : '',
            date :'',
            quatity : 0,
            unit : 0,
           priceUnit : 0,
           vat: 0,
           price : 0,
           priceHT : 0
          }
        ]
   },
   estimate:'',
   loadPage:false,
   estimates:[]
  }
export function estimate(state = initialState, action) {
                switch (action.type) {
                  case SEARCH_START:{
                    return{
                       ...state,
                       loadPage:true
                    }
                  }
                  case FETECHED_ESTIMATES:{
                    console.log(action.products);  
                    return {
                      ...state,
                      currentPageEstimate:1,
                      estimates:action.estimates,
                      dataCustomersEstimate:action.customers,
                      dataProductsEstimate:action.products,
                      loadPage:false,
                      searchEstimate:'',
                      detailEstimate:''
                    }
                    }
                    case ID_PDF_ESTIMATE:{
                      return{
                        ...state,
                        dataPdfEstimate:action.estimate
                      }
                    }
                    case ORDER_ESTIMATES:{
                      const estimates=_.orderBy(state.estimate,action.valeur,"asc");
                      console.log(estimates);
                      return{
                        ...state,
                        estimates:estimates
                      }
                    }
                    case EDIT_ESTIMATE:{
                      const lgt=state.detailEstimate.products.length;
                      const tab=[];
                      for(let i=0;i<lgt;i++){
                        tab.push(i);
                      }
                      console.log(tab);
                      const d1=state.detailEstimate.devisDate;
                      const d2=state.detailEstimate.dateValidity;
                      return{
                        ...state,
                        length:lgt,
                        table:tab,
                        startDate1:d1,
                        startDate2:d2
                      }
                    }
                    case FILTER_LIST_ESTIMATE:{
                      const estimates=state.estimates.filter((estimate=>estimate._id!==state.detailEstimate._id))
                    return{
                      ...state,
                      estimates:estimates,
                      detailEstimate:''
                    }
                    }
                    case CHANGE_DATE1:{
                      return{
                        ...state,
                        startDate1:action.date
                      }
                    }
                    case CHANGE_DATE2:{
                      return{
                        ...state,
                        startDate2:action.date
                      }
                    }
                    case CHANGE_DATE_ESTIMATE:{
                      const product=state.dataEstimate;
                     product.products[action.val].date=action.date;
                     return{
                       ...state,
                       dataEstimate:product
                     } 
                    }
                    case SEARCH_ESTIMATES:{
                      return{
                        ...state,
                        searchEstimate:action.name
                      }
                    }
                    case DELETE_ROW_PRODUCT:{
                     let invoice=state.dataEstimate;
                     let start=action.val-1;
                     let end=action.val;
                     console.log(end);
                     invoice.products=invoice.products.slice(start,end);
                     invoice.totalTTC=0;
                     invoice.net=0;
                     invoice.products.map((prod)=>{
                      invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                      invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                     })
                     console.log(invoice);
                    return {
                      ...state,
                      dataEstimate:invoice
                    }
                    }
                    case DELETE_ROW_DETAIL_PRODUCT:{
                      let invoice=state.detailEstimate;
                      let start=action.val-1;
                      let end=action.val;
                      console.log(end);
                      invoice.products=invoice.products.slice(start,end);
                      invoice.totalTTC=0;
                      invoice.net=0;
                      invoice.products.map((prod)=>{
                       invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                       invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                      })
                      const tab=state.table.filter(value=>value!==action.val);
                      const lgt=state.length-1;
                      console.log(invoice);
                     return {
                       ...state,
                       detailEstimate:invoice,
                       table:tab,
                       length:lgt
                     }
                    }
                    case DETAIL_ESTIMATES:{
                      return{
                        ...state,
                        detailEstimate:action.estimate
                      }
                    }
                    case PAGINATION_ESTIMATE:{
                      console.log(action.nbrPage);
                      return {
                      ...state,
                      currentPageEstimate:action.nbrPage
                      
                    }
                  }
                  case NEW_ESTIMATE:{
                    console.log(action.data);
                    let lgt=state.estimates.length;
                    let invoice=state.dataEstimate;
                    invoice.message=action.data.message;
                    invoice.notesFooter=action.data.notesFooter;
                    return{
                      ...state,
                      dataEstimate:invoice,
                      numberEstimate:lgt
                    }
                  }
                  case UPDATE_ESTIMATE_FAILED:{
                    return{
                      ...state,
                      invoice:"something wrong"
                    }
                  }
                  case CHANGE_DATA_ESTIMATE:{
                    const invoice=state.dataEstimate;
                    invoice[action.name]=action.value;
                    return{
                      ...state,
                      dataEstimate:invoice
                    }
                  }
                  case CHANGE_DETAIL_ESTIMATE:{
                      const estimate=state.detailEstimate;
                      estimate[action.name]=action.value;
                      return{
                        ...state,
                        detailEstimate:estimate
                      }
                    }
                  case ADD_ROW_PRODUCTS:{
                  const product=state.dataEstimate;
                    product.products.push({
                    id_product : '',
                     name : '',
                     date :new Date(),
                    quatity : 1,
                    unit : 0,
                   priceUnit : 0,
                   vat: 0,
                   price : 0,
                   priceHT : 0
                  });
                  console.log(product);
                  return{
                    ...state,
                    dataEstimate:product
                  }
               
              }
              case ADD_DETAILPRODUCT_ROW_PRODUCT:{
                const product=state.detailEstimate;
                    product.products.push({
                    id_product : '',
                     name : '',
                     date :new Date(),
                    quatity : 1,
                    unit : 0,
                   priceUnit : 0,
                   vat: 0,
                   price : 0,
                   priceHT : 0
                  });
                  const lgt=state.length+1;
                  let tab=[];
                  for(let i=0;i<lgt;i++){
                    tab.push(i);
                  }
                  return{
                    ...state,
                    detailEstimate:product,
                    length:lgt,
                    table:tab
                  }
              }
                  case CHANGE_DATA_ESTIMATE_PRODUCTS:{
                    console.log(action.products);
                    const {name,value,index,products}=action;
                    console.log(state.dataEstimate.priceTTC);
                   const invoice=state.dataEstimate;
                   let product=invoice.products[index];
                      
                  
                  if(name==="name"){
                   product.name=value;
                    products.map((prod)=>{
                     console.log(prod);
                      if(prod.name===value){
                    product.priceUnit=prod.priceWT;
                  product.unit=prod.unit;
                    product.vat=prod.vat;       
                    product.price=prod.priceWT;
                    product.priceHT=prod.priceTTC;

                  }
                })
              }
                   if(name==="quatity"){
                    product.quatity=value;
                   }
                   if(product.quatity>0){
                   product.priceHT=product.priceHT*product.quatity;
                   }
                   invoice.totalTTC=0;
                   invoice.net=0;
                   invoice.products.map((prod)=>{
                    invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                    invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                   })
                   console.log(invoice.totalTTC);
                   return{
                    ...state,
                    dataEstimate:invoice
                  }
                  }
                  case CHANGE_DETAIL_ESTIMATE_PRODUCTS:{
                    console.log(action.products);
                    const {name,value,index,products}=action;
                    console.log(state.dataEstimate.priceTTC);
                   const invoice=state.detailEstimate;
                   let product=invoice.products[index];
                  if(name==="name"){
                   product.name=value;
                    products.map((prod)=>{
                     console.log(prod);
                      if(prod.name===value){
                    product.priceUnit=prod.priceWT;
                  product.unit=prod.unit;
                    product.vat=prod.vat;       
                    product.price=prod.priceWT;
                    product.priceHT=prod.priceTTC;

                  }
                })
              }
                   if(name==="quatity"){
                    product.quatity=value;
                   }
                   if(product.quatity>0){
                   product.priceHT=product.priceHT*product.quatity;
                   }
                   invoice.totalTTC=0;
                   invoice.net=0;
                   invoice.products.map((prod)=>{
                    invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                    invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                   })
                   console.log(invoice.totalTTC);
                   return{
                    ...state,
                    detailEstimate:invoice
                  }
                  }
                  case UPDATE_ESTIMATE:{
                    const newInvoices=state.estimates;
                    const index=newInvoices.indexOf(state.detailEstimate);
                    console.log(index);
                    console.log(action.estimate);
                    const invoice=action.estimate;
                    newInvoices[index]={...invoice};
                    console.log(newInvoices);
                    return{ 
                      ...state,
                      estimate:'',
                      detailEstimate:'',
                    estimates:newInvoices,
                    dataPdfEstimate:action.estimate
                    }
                  }
                  case ADD_ESTIMATE:{
                   
                    state.estimates.push(action.invoice);
                    const newInvoices=state.estimates;
                    return{
                      ...state,
                      dataEstimate:{
                        companyName: '',
                        billNumber : 0,
                        billDate : '',
                        conditionPayment : 0,
                        deadline :'',
                        message : '',
                        notesFooter : '',
                        discount : 0,
                        totalTTC : 0,
                        products : [ 
                            {
                                id_product : '',
                                name : '',
                                date :'',
                                quatity : 0,
                                unit : 0,
                                priceUnit : 0,
                                vat: 0,
                                price : 0,
                                priceHT : 0
                            }    
                        ] 
                     },
                     invoices:newInvoices,
                     invoice:''
                    }
                  }
                  case CHANGE_VALUE_ESTIMATE:{
                    const invoice=state.detailEstimate;
                    invoice[action.name]=action.value;
                    return{
                      ...state,
                      detailEstimate:invoice
                    }
                  }
                 case CHANGE_DATE_DETAIL_ESTIMATE_PRODUCT:{
                   const product=state.detailEstimate.products;
                   product[action.val].date=action.date;
                   return{
                     ...state,
                     detailEstimate:product
                   }
                 }
                  case DELETE_ESTIMATE:{
                    const newInvoices=state.estimates.filter(product=>product._id!==action.id);
                    console.log(newInvoices);
                  return{
                    ...state,
                    estimates:newInvoices,
                    detailEstimate:''
                  }
                  }
                  default:{
                    return state
                }
     }
    }