import {FETECHED_HOME_INVOICE} from '../_actions/types';
const initialeState={
height:300,
width:300,
dataHome:[],
paid:0,
created:0,
overdue:0
}
export function home(state=initialeState,action){
    switch (action.type){
    case FETECHED_HOME_INVOICE:{
      return{
          ...state,
          dataHome:action.data,
          paid:action.paid,
          created:action.created,
          overdue:action.overdue
      }
    }
    default:{
        return state;
    }
}
}