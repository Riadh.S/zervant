import { LOCALE_SET } from "./../_actions/types";

const INITIAL_STATE = { lang: "en" };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOCALE_SET:
      return {
        lang: action.lang
      };

    default:
      return state;
  }
};
