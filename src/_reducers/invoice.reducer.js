import {FETECHED_ALL_INVOICE,ID_PDF,CHANGE_DATE,PUT_DETAIL_INVOICE,DELETE_ROW_DETAIL_INVOICE_PRODUCT,CHANGE_DATE_DETAIL_INVOICE_PRODUCT,CHANGE_DETAIL_INVOICE_PRODUCTS,ADD_ROW_DETAIL_INVOICE,EDIT_INVOICE,CHANGE_DETAIL_INVOICE,CHANGE_DATE11,CHANGE_DATE22,DELETE_ROW,ADD_INVOICE,ADD_ROW_PRODUCT,NEW_INVOICE,CHANGE_DATA_INVOICE_PRODUCTS,FETCH_START,ORDER_INVOICES,CHANGE_DATA_INVOICE,SEARCH_INVOICES,DELETE_INVOICE,DETAIL_INVOICES,PAGINATION_INVOICE,UPDATE_INVOICE,UPDATE_INVOICE_FAILED} from '../_actions/types';
import _ from 'lodash';

const initialState={
  searchInvoice:'',
        detailInvoice:'',
        listInvoice:4,
        currentPageInvoice:1,
        dataPdf:'',  
        startDate1: new Date(),
        startDate2: new Date(),
        length:0,
        table:[],
        numberInvoice:0,
        productsData:[],
        customersData:[],    
     dataInvoice:{
        companyName: '',
        billNumber : 0,
        billDate : '',
        conditionPayment : 0,
        deadline :'',
        message : '',
        notesFooter : '',
        net:0,
        discount : 0,
        totalTTC : 0,
        products:[
          {
            id_product : '',
             name : '',
            date :new Date(),
            quatity : 1,
            unit : '',
            date :'',
            quatity : 0,
            unit : 0,
           priceUnit : 0,
           vat: 0,
           price : 0,
           priceHT : 0
          }
        ]
   },
   invoice:'',
   load:false,
   invoices:[]
  }
export function invoice(state = initialState, action) {
                switch (action.type) {
                  case FETCH_START:{
                    return{
                       ...state,
                       load:true
                    }
                  }
                  case FETECHED_ALL_INVOICE:{
                    return {
                      ...state,
                      currentPageInvoice:1,
                      invoices:action.invoices,
                      productsData:action.products,
                      customersData:action.customers,
                      load:false,
                      searchInvoice:'',
                      detailInvoice:''
                    }
                    }
                    case ID_PDF:{
                      return{
                        ...state,
                        dataPdf:action.invoice
                      }
                    }
                    case ADD_ROW_DETAIL_INVOICE:{
                      const product=state.detailInvoice;
                    product.products.push({
                    id_product : '',
                     name : '',
                    date :new Date(),
                    quatity : 1,
                    unit : 0,
                   priceUnit : 0,
                   vat: 0,
                   price : 0,
                   priceHT : 0
                  });
                  const lgt=state.length+1;
                  let tab=[];
                  for(let i=0;i<lgt;i++){
                    tab.push(i);
                  }
                  console.log(product);
                  return{
                    ...state,
                    detailInvoice:product,
                    table:tab,
                    length:lgt
                  }
                    }
                    case CHANGE_DETAIL_INVOICE_PRODUCTS:{
                      console.log(action.products);
                      const {name,value,index,products}=action;
                      console.log(state.dataEstimate.priceTTC);
                     const invoice=state.detailInvoice;
                     let product=invoice.products[index];
                    if(name==="name"){
                     product.name=value;
                      products.map((prod)=>{
                        if(prod.name===value){
                      product.priceUnit=prod.priceWT;
                    product.unit=prod.unit;
                      product.vat=prod.vat;       
                      product.price=prod.priceWT;
                      product.priceHT=prod.priceTTC;
  
                    }
                  })
                }
                     if(name==="quatity"){
                      product.quatity=value;
                     }
                     if(product.quatity>0){
                     product.priceHT=product.priceHT*product.quatity;
                     }
                     invoice.totalTTC=0;
                     invoice.net=0;
                     invoice.products.map((prod)=>{
                      invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                      invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                     })
                     console.log(invoice.totalTTC);
                     return{
                      ...state,
                      detailInvoice:invoice
                    }
                    }
                    case DELETE_ROW_DETAIL_INVOICE_PRODUCT:{
                      let invoice=state.detailInvoice;
                      let start=action.val-1;
                      let end=action.val;
                      console.log(end);
                      invoice.products=invoice.products.slice(start,end);
                      invoice.totalTTC=0;
                      invoice.net=0;
                      invoice.products.map((prod)=>{
                       invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                       invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                      })
                      const tab=state.table.filter(value=>value!==action.val);
                      const lgt=state.length-1;
                      console.log(invoice);
                     return {
                       ...state,
                       detailInvoice:invoice,
                       table:tab,
                       length:lgt
                     }
                    }
                    case ORDER_INVOICES:{
                      const invoices=_.orderBy(state.invoices,action.valeur,"asc");
                      console.log(invoices);
                      return{
                        ...state,
                        invoices:invoices
                      }
                    }
                    case CHANGE_DATE:{
                      const product=state.dataInvoice;
                     product.products[action.val].date=action.date;
                     return{
                       ...state,
                       dataInvoice:product
                     } 
                    }
                    case EDIT_INVOICE:{
        
                      const lgt=state.detailInvoice.products.length;
                      const tab=[];
                      for(let i=0;i<lgt;i++){
                        tab.push(i);
                      }
                      console.log(tab);
                      const d1=state.detailInvoice.billDate;
                      const d2=state.detailInvoice.deadline
                      return{
                        ...state,
                        length:lgt,
                        table:tab,
                        startDate1:d1,
                        startDate2:d2
                      }
                    }
                    case SEARCH_INVOICES:{
                      return{
                        ...state,
                        searchInvoice:action.name
                      }
                    }
                    case CHANGE_DATE11:{
                      return{
                        ...state,
                        startDate1:action.date
                      }
                    }
                    case CHANGE_DATE22:{
                      return{
                        ...state,
                        startDate2:action.date
                      }
                    }
                    case DELETE_ROW:{
                     let invoice=state.dataInvoice;
                     let start=action.val-1;
                     let end=action.val;
                     console.log(end);
                     invoice.products=invoice.products.slice(start,end);
                     invoice.totalTTC=0;
                     invoice.net=0;
                     invoice.products.map((prod)=>{
                      invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                      invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                     })
                     console.log(invoice);
                    return {
                      ...state,
                      dataInvoice:invoice
                    }
                    }
                    case DETAIL_INVOICES:{
                      return{
                        ...state,
                        detailInvoice:action.invoice
                      }
                    }
                    case PAGINATION_INVOICE:{
                      console.log(action.nbrPage);
                      return {
                      ...state,
                      currentPageInvoice:action.nbrPage
                      
                    }
                  }
                  case NEW_INVOICE:{
                    console.log(action.data);
                    let invoice=state.dataInvoice;
                    invoice.message=action.data.message;
                    invoice.notesFooter=action.data.notesFooter;
                    let lgt=state.invoices.length;
                    return{
                      ...state,
                      dataInvoice:invoice,
                      numberInvoice:lgt
                    }
                  }
                  case UPDATE_INVOICE_FAILED:{
                    return{
                      ...state,
                      invoice:"something wrong"
                    }
                  }
                  case CHANGE_DATA_INVOICE:{
                    const invoice=state.dataInvoice;
                    invoice[action.name]=action.value;
                    return{
                      ...state,
                      dataInvoice:invoice
                    }
                  }
                  case ADD_ROW_PRODUCT:{
                  const product=state.dataInvoice;
                    product.products.push({
                    id_product : '',
                     name : '',
                    date :new Date(),
                    quatity : 1,
                    unit : 0,
                   priceUnit : 0,
                   vat: 0,
                   price : 0,
                   priceHT : 0
                  });
                  console.log(product);
                  return{
                    ...state,
                    dataInvoice:product
                  }
               
              }
              case CHANGE_DATE_DETAIL_INVOICE_PRODUCT:{
                const product=state.detailInvoice;
                product.products[action.val].date=action.date;
                return{
                  ...state,
                  detailInvoice:product
                }
              }
                  case CHANGE_DATA_INVOICE_PRODUCTS:{
                    console.log(action.products);
                    const {name,value,index,products}=action;
                    console.log(state.dataInvoice.priceTTC);
                   const invoice=state.dataInvoice;
                   let product=invoice.products[index];
                      
                  
                  if(name==="name"){
                   product.name=value;
                    products.map((prod)=>{
                     console.log(prod);
                      if(prod.name===value){
                    product.priceUnit=prod.priceWT;
                  product.unit=prod.unit;
                    product.vat=prod.vat;       
                    product.price=prod.priceWT;
                    product.priceHT=prod.priceTTC;

                  }
                })
              }
                   if(name==="quatity"){
                    product.quatity=value;
                   }
                   if(product.quatity>0){
                   product.priceHT=product.priceHT*product.quatity;
                   }
                   invoice.totalTTC=0;
                   invoice.net=0;
                   invoice.products.map((prod)=>{
                    invoice.totalTTC=invoice.totalTTC+prod.priceHT;
                    invoice.net=invoice.net+(prod.priceUnit*prod.quatity);
                   })
                   console.log(invoice.totalTTC);
                   return{
                    ...state,
                    dataInvoice:invoice
                  }
                  }
                  case UPDATE_INVOICE:{
                    const newInvoices=state.invoices;
                    const index=newInvoices.indexOf(state.detailInvoice);
                    console.log(index);
                    console.log(action.invoice);
                    const invoice=action.invoice;
                    newInvoices[index]={...invoice};
                    console.log(newInvoices);
                    return{ 
                      ...state,
                      invoice:'',
                      detailInvoice:'',
                    invoices:newInvoices,
                    dataPdf:action.invoice
                    }
                  }
                  case PUT_DETAIL_INVOICE:{
                    const newInvoices=state.invoices;
                    const index=newInvoices.indexOf(state.detailInvoice);
                    console.log(action.invoice);
                    console.log(action.newData);
                    const invoice=action.newData;
                    invoice.status=action.invoice.status;
                    invoice.paid=action.invoice.paid;
                    console.log(invoice);
                    newInvoices[index]={...invoice};
                    console.log(newInvoices);
                    return{ 
                      ...state,
                      invoice:'',
                      detailInvoice:invoice,
                    invoices:newInvoices
                    }
                  }
                  case ADD_INVOICE:{
                   
                    state.invoices.push(action.invoice);
                    const newInvoices=state.invoices;
                    return{
                      ...state,
                      data:{
                        companyName: '',
                        billNumber : 0,
                        billDate : '',
                        conditionPayment : 0,
                        deadline :'',
                        message : '',
                        notesFooter : '',
                        discount : 0,
                        totalTTC : 0,
                        products : [ 
                            {
                                id_product : '',
                                name : '',
                                date :'',
                                quatity : 0,
                                unit : 0,
                                priceUnit : 0,
                                vat: 0,
                                price : 0,
                                priceHT : 0
                            }    
                        ] 
                     },
                     invoices:newInvoices,
                     invoice:''
                    }
                  }
                  case CHANGE_DETAIL_INVOICE:{
                    const invoice=state.detailInvoice;
                    invoice[action.name]=action.value;
                    return{
                      ...state,
                      detailInvoice:invoice
                    }
                  };
                  case DELETE_INVOICE:{
                    const newInvoices=state.invoices.filter(product=>product._id!==action.id);
                    console.log(newInvoices);
                  return{
                    ...state,
                    invoices:newInvoices,
                    detailInvoice:''
                  }
                  }
                  default:{
                    return state
                }
     }
    }