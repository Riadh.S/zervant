import {FETECHED_USERS,LIST_SUPPORT,PAGINATION_USER,DELETE_USER,PAGINATION_SUPPORT,MESSAGE_FROM_SERVER} from '../_actions/types';
const initialeState={
    users:[],
    listUser:4,
    currentPageUser:1,
    support:[],
    listSupport:4,
    currentPageSupport:1,
    newMessage:0,
    detailUser:''
}
export function admin(state=initialeState,action){
    switch(action.type){
        case FETECHED_USERS:{
            return{
                ...state,
                listUser:4,
               currentPageUser:1,
                users:action.data
            }
        }
        case PAGINATION_USER:{
            return{
                ...state,
                currentPageUser:action.nbrPage
            } 
        }
        case PAGINATION_SUPPORT:{
            return{
                ...state,
              currentPageSupport:action.nbrPage 
            }
        }
        case MESSAGE_FROM_SERVER:{
            let messages=state.support;
            messages.push(action.messsage);
            let msg=state.newMessage;
            msg=msg++;
            return{
                ...state,
                support:messages,
                newMessage:msg
            
            }
        }
        case LIST_SUPPORT:{
            return{
               ...state,
               support:action.support 
            }
        }
        case DELETE_USER:{
            let users=state.users.filter((user)=>user._id!==action.user._id);
            return{
                ...state,
                users:users
            }
        }
        default:{
            return state;
        }
    }
}