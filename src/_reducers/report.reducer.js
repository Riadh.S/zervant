import {FETECHED_REPORT_INVOICE,ORDER_LIST,FILTER_REPORT_INVOICE} from "../_actions/types";
let initialeState={
    dataSalesReport:[],
    invoicesSalesReport:[],
    reportType:'customers',
    loadData:false,
    filterData:false
}
export function report(state=initialeState,action){
    switch(action.type){
        case FETECHED_REPORT_INVOICE:{
         console.log(action.invoices);
         return{
             ...state,
             dataSalesReport:action.data,
             invoicesSalesReport:action.invoices,
             loadData:true,
             filterData:false
         }
     }
     case ORDER_LIST:{
        return{ 
        ...state,
         reportType:action.order
        }
     }
     case FILTER_REPORT_INVOICE:{
         return{
         ...state,
         dataSalesReport:action.data,
         invoicesSalesReport:action.newInvoices,
         filterData:true
         }
     }
     default:{
         return state;
     }
    }
}