import React, { Component } from "react";
import {
  Grid,
  Table,
  Menu,
  Icon,
  Label,
  Segment,
  Header,
  Input,
  Button,
  Sidebar,
  Message,
  Divider,
  Modal
} from "semantic-ui-react";
import ProductForm from "./ProductForm";
import { getProducts, deleteProduct } from "../../_actions/product.actions";
import { connect } from "react-redux";
import { paginate } from "../../utils/paginate";
import Pagination from "../../commons/Pagination";
import _ from "lodash";

export class Products extends Component {
  state = {
    visible: true,
    modal: false,
    searchTerm: "",
    currentPage: 1,
    pageSize: 4,
    sortColumn: { column: "name", order: "asc" },
    product: "",
    deleteConfirm: false,
    edit: false
  };

  componentWillMount() {
    this.props.getProducts();
  }

  openModal = () => this.setState({ modal: true });

  closeModal = () => this.setState({ modal: false, edit: false });

  openEditModal = () => this.setState({ modal: true, edit: true });

  handleAnimationChange = () => () =>
    this.setState({ visible: !this.state.visible });

  handleSearchChange = e =>
    this.setState({ [e.target.name]: e.target.value, currentPage: 1 });

  handlePageChange = page => this.setState({ currentPage: page });

  handleProductDetails = product => this.setState({ product });

  handleSort = column => {
    var sortColumn = { column };
    if (column === this.state.sortColumn.column)
      sortColumn.order = this.state.sortColumn.order === "asc" ? "desc" : "asc";
    else {
      sortColumn.order = "asc";
    }
    this.setState({ sortColumn, currentPage: 1 });
  };

  renderSortIcon = column => {
    const { sortColumn } = this.state;

    if (column !== sortColumn.column) return <Icon name="sort" />;
    if (sortColumn.order === "asc") return <Icon name="sort up" />;
    return <Icon name="sort down" />;
  };

  openDeleteModal = () => this.setState({ deleteConfirm: true });
  closeDeleteModal = () => this.setState({ deleteConfirm: false });

  handleDelete = id => {
    this.props.deleteProduct(id);
    this.closeDeleteModal();
    this.props.getCustomers();
  };

  render() {
    const {
      visible,
      searchTerm,
      currentPage,
      pageSize,
      sortColumn,
      product,
      deleteConfirm,
      edit
    } = this.state;

    const { products: allProducts } = this.props;

    const productsData =
      searchTerm !== ""
        ? allProducts.filter(product => product.name.startsWith(searchTerm))
        : allProducts;
    const totalCount = productsData.length;

    const sortedProducts = _.orderBy(
      productsData,
      [sortColumn.column],
      [sortColumn.order]
    );
    const products = paginate(sortedProducts, currentPage, pageSize);

    return (
      <Grid divided="vertically">
        <Grid.Row columns={1}>
          <Grid.Column>
            <Segment clearing>
              <Header
                fluid="true"
                as="h2"
                floated="left"
                style={{ marginBottom: 0 }}
              >
                <span>
                  Products
                  <Icon name="product hunt" color="black" />
                </span>
                <Header.Subheader>
                  {allProducts.length} products
                </Header.Subheader>
              </Header>
              <Header floated="right">
                <Input
                  // loading={searchLoading}
                  onChange={this.handleSearchChange}
                  size="mini"
                  icon="search"
                  name="searchTerm"
                  placeholder="Search Messages"
                />
              </Header>
              <Button.Group>
                <Button onClick={this.handleAnimationChange()}>
                  Show sidebar
                </Button>
                <Button onClick={this.openModal}>New Product</Button>
                <Button onClick={this.handleAnimationChange()}>Export</Button>
              </Button.Group>
            </Segment>

            <Sidebar.Pushable style={{ height: "60vh" }}>
              <Sidebar
                as={Grid}
                direction="right"
                animation="overlay"
                icon="labeled"
                vertical
                visible={visible}
                style={{ width: "50%", paddingRight: 0 }}
              >
                <Grid
                  style={{
                    background: "#fff",
                    width: "100%",
                    marginLeft: 0
                  }}
                  textAlign="center"
                  verticalAlign="middle"
                >
                  <Grid.Column fluid>
                    {!product && (
                      <Icon
                        name="file alternate outline"
                        size="huge"
                        bordered
                      />
                    )}
                    {product && (
                      <React.Fragment>
                        <Icon
                          name="dolly flatbed"
                          size="big"
                          color="black"
                          circular
                        />
                        <Header as="h1" textAlign="center">
                          {product.name}
                        </Header>
                        <Header sub>
                          Description : <span> {product.description}</span>
                        </Header>
                        <Header sub>
                          Unit : <span> {product.unit}</span>
                        </Header>
                        <Header sub>
                          Price based on : <span> {product.priceBasedOn}</span>
                        </Header>
                        <Header sub>
                          Price WT : <span> {product.priceWT}</span>
                        </Header>
                        <Header sub>
                          VAT : <span> {product.vat}%</span>
                        </Header>
                        <Header sub>
                          Price TTC : <span> {product.priceTTC}</span>
                        </Header>
                        <Header sub>
                          Creation date : <span> {product.createdAt}</span>
                        </Header>
                      </React.Fragment>
                    )}

                    {product && (
                      <React.Fragment>
                        <Divider section />
                        <Button
                          onClick={this.openEditModal}
                          className="editButton"
                        >
                          <Icon name="edit" /> Edit
                        </Button>
                        <Button
                          className="deleteButton"
                          onClick={this.openDeleteModal}
                        >
                          <Icon name="delete" /> Delete
                        </Button>
                      </React.Fragment>
                    )}
                  </Grid.Column>
                </Grid>
              </Sidebar>

              <Sidebar.Pusher>
                <Table celled style={{ width: visible ? "50%" : "100%" }}>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("name")}
                        >
                          Name {this.renderSortIcon("name")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("description")}
                        >
                          Description {this.renderSortIcon("description")}
                        </label>
                      </Table.HeaderCell>
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("unit")}
                          >
                            Unit {this.renderSortIcon("unit")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("priceBasedOn")}
                          >
                            Price based on {this.renderSortIcon("priceBasedOn")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("priceWt")}
                        >
                          Price WT {this.renderSortIcon("priceWt")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("vat")}
                        >
                          VAT {this.renderSortIcon("vat")}
                        </label>
                      </Table.HeaderCell>
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("priceTTC")}
                          >
                            Price TTC {this.renderSortIcon("priceTTC")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("createdAt")}
                          >
                            Creation date {this.renderSortIcon("createdAt")}
                          </label>
                        </Table.HeaderCell>
                      )}
                    </Table.Row>
                  </Table.Header>

                  <Table.Body>
                    {products.map(product => (
                      <Table.Row
                        key={product._id}
                        onClick={() => this.handleProductDetails(product)}
                      >
                        <Table.Cell>{product.name}</Table.Cell>
                        <Table.Cell>{product.description}</Table.Cell>
                        {!visible && <Table.Cell>{product.unit}</Table.Cell>}
                        {!visible && (
                          <Table.Cell>{product.priceBasedOn}</Table.Cell>
                        )}
                        <Table.Cell>{product.priceWT}</Table.Cell>
                        <Table.Cell>{product.vat}%</Table.Cell>
                        {!visible && (
                          <Table.Cell>{product.priceTTC}</Table.Cell>
                        )}
                        {!visible && (
                          <Table.Cell>{product.createdAt}</Table.Cell>
                        )}
                      </Table.Row>
                    ))}
                  </Table.Body>

                  <Table.Footer>
                    <Table.Row>
                      <Table.HeaderCell colSpan={visible ? 4 : 8}>
                        <Pagination
                          itemsCount={totalCount}
                          pageSize={pageSize}
                          currentPage={currentPage}
                          onPageChange={this.handlePageChange}
                        />
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Footer>
                </Table>
              </Sidebar.Pusher>
            </Sidebar.Pushable>
            <ProductForm
              show={this.state.modal}
              closeModal={this.closeModal}
              product={edit ? product : null}
              getProducts={this.props.getProducts}
            />
            <Modal
              size="tiny"
              basic
              open={deleteConfirm}
              onClose={this.closeDeleteModal}
            >
              <Modal.Header as="h1" icon color="orange" textAlign="center">
                <Icon name="dolly flatbed" color="orange" />
                {"    "}
                Confirmation
              </Modal.Header>
              <Modal.Content>
                <Segment stacked textAlign="left" style={{ padding: "3em" }}>
                  <Header sub>
                    Are you sure you want to delete this product ?
                  </Header>
                  <span>$10.99</span>
                  <Divider hidden />
                </Segment>
                <Button
                  floated="right"
                  className="deleteButton-inverted"
                  inverted
                  size="large"
                  onClick={() => this.handleDelete(product._id)}
                >
                  <Icon name="remove" /> Delete
                </Button>
                <Button
                  floated="right"
                  color="grey"
                  inverted
                  size="large"
                  onClick={this.closeDeleteModal}
                >
                  <Icon name="dont" /> Cancel
                </Button>
              </Modal.Content>
              <Modal.Actions />
            </Modal>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = ({ product }) => {
  const { products } = product;
  return { products };
};

export default connect(
  mapStateToProps,
  { getProducts, deleteProduct }
)(Products);
