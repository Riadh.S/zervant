import React, { Component } from "react";
import {
  Modal,
  Form,
  Grid,
  Segment,
  Button,
  Icon,
  Dropdown,
  Label,
  Message
} from "semantic-ui-react";
import { connect } from "react-redux";
import JoiForm from "../../commons/Form";
import { calculateTTC } from "../../_services/GlobalFunctions";
import { FormattedMessage } from "react-intl";
import Joi from "joi";
import message from "../../translations";
import { getIdUser } from "../../_services/UserServices";
import { addProduct, updateProduct } from "../../_actions/product.actions";
import _ from "lodash";

const options = [
  {
    key: "price including VAT",
    text: "price including VAT",
    value: "price including VAT"
  },
  {
    key: "price excluding VAT",
    text: "price excluding VAT",
    value: "price excluding VAT"
  }
];
export class ProductForm extends JoiForm {
  schema = {
    name: Joi.string()
      .required()
      .error(() => message[this.props.lang]["error-message-name"]),
    description: Joi.string()
      .required()
      .error(() => message[this.props.lang]["error-message-description"]),
    unit: Joi.string()
      .required()
      .error(() => message[this.props.lang]["error-message-unit"]),
    priceWT: Joi.number()
      .required()
      .error(() => message[this.props.lang]["error-message-priceWT"]),
    vat: Joi.number()
      .required()
      .error(() => message[this.props.lang]["error-message-vat"]),
    priceBasedOn: Joi.required().error(
      () => message[this.props.lang]["error-message-priceBasedOn"]
    )
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.product) {
      let data = { ...nextProps.product };
      const productId = data._id;
      data = _.omit(data, [
        "__v",
        "createdAt",
        "id_user",
        "updatedAt",
        "priceTTC",
        "_id"
      ]);
      this.setState({ data, productId });
    } else {
      const data = {};
      const errors = {};
      const productId = null;
      this.setState({ data, errors, productId });
    }
  }

  doSubmit() {
    const { data, productId } = this.state;
    let product = {
      ...data,
      // _id: productId,
      id_user: getIdUser(),
      priceTTC: calculateTTC(data)
    };
    if (!this.props.product) this.props.addProduct(product);
    else this.props.updateProduct(productId, product);
    this.props.getProducts();
    this.closeModal();
  }

  closeModal = () => {
    this.props.closeModal();
    this.setState({ data: {} });
  };

  render() {
    const { errors } = this.state;
    const { product } = this.props;

    return (
      <Modal size="tiny" basic open={this.props.show} onClose={this.closeModal}>
        <Modal.Header as="h1" icon color="orange" textAlign="center">
          <Icon name="dolly flatbed" color="orange" />
          {"    "}
          {product ? "Edit Product" : "Add a product"}
        </Modal.Header>
        <Modal.Content>
          <Grid textAlign="center" verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 600 }} textAlign="left">
              <Form error size="large" onSubmit={this.handleSubmit}>
                <Segment stacked textAlign="left" style={{ padding: "3em" }}>
                  {this.inputForm("Name", "name")}
                  {this.inputForm("Description", "description")}
                  {this.inputForm("Unit", "unit")}
                  <Form.Field>
                    <label>Price based on</label>
                    <Form.Dropdown
                      name="priceBasedOn"
                      floating
                      button
                      placeholder="Price based on"
                      fluid
                      options={options}
                      onChange={this.customHandleChange}
                      defaultValue={
                        this.state.data.priceBasedOn &&
                        this.state.data.priceBasedOn
                      }
                    />
                  </Form.Field>
                  {errors["priceBasedOn"] && (
                    <Message
                      style={{ marginTop: "-1em" }}
                      error
                      content={errors["priceBasedOn"]}
                    />
                  )}
                  {this.inputForm("Price WT", "priceWT")}
                  {this.inputForm("VAT", "vat")}
                  <Form.Field>
                    <Form.Input
                      disabled={
                        this.state.data.priceBasedOn === "price excluding VAT"
                          ? true
                          : false
                      }
                      name="priceTTC"
                      type="text"
                      value={
                        this.state.data.priceBasedOn &&
                        this.state.data.priceWT &&
                        calculateTTC(this.state.data)
                          ? calculateTTC(this.state.data)
                          : ""
                      }
                      fluid
                      label="price TTC"
                    />
                  </Form.Field>
                  {errors["priceTTC"] && (
                    <Message
                      style={{ marginTop: "-1em" }}
                      error
                      content={errors["priceTTC"]}
                    />
                  )}
                </Segment>

                {this.buttonSubmit("Submit")}
                <Button
                  floated="right"
                  color="red"
                  inverted
                  size="large"
                  onClick={this.closeModal}
                >
                  <Icon name="remove" /> Cancel
                </Button>
              </Form>
            </Grid.Column>
          </Grid>
        </Modal.Content>
        <Modal.Actions />
      </Modal>
    );
  }
}

const mapStateToProps = ({ locale }) => {
  const { lang } = locale;
  return { lang };
};

export default connect(
  mapStateToProps,
  { addProduct, updateProduct }
)(ProductForm);
