import React, { Component } from "react";
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon
} from "semantic-ui-react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Joi from "joi";
import JoiForm from "../../commons/Form";
import { FormattedMessage } from "react-intl";
import message from "../../translations";
import { register } from "../../_actions";

export class Register extends JoiForm {
  state = {
    data: {
      name: "",
      email: "",
      password: "",
      passwordConfirmation: "",
      companyName: ""
    },
    errors: ""
  };

  schema = {
    name: Joi.string()
      .required()
      .error(() => message[this.props.locale.lang]["error-message-name"]),
    email: Joi.string()
      .email()
      .required()
      .error(
        () => message[this.props.locale.lang]["error-message-emailCustomer"]
      ),
    password: Joi.string()
      .min(8)
      .required()
      .error(
        () => message[this.props.locale.lang]["error-message-password-register"]
      ),
    passwordConfirmation: Joi.string()
      .min(8)
      .required()
      .error(
        () => message[this.props.locale.lang]["error-message-password-register"]
      ),
    companyName: Joi.string()
      .required()
      .error(() => message[this.props.locale.lang]["error-message-CompanyName"])
  };

  isPasswordValid = ({ password, passwordConfirmation }) => {
    if (password !== passwordConfirmation) {
      const errors = {};
      errors.passwordConfirmation =
        message[this.props.locale.lang][
          "error-message-password-register-not-the-same"
        ];
      this.setState({ errors });
      return false;
    }
  };

  async doSubmit() {
    this.isPasswordValid(this.state.data);
    if (!this.state.errors && !this.isPasswordValid(this.state.data)) {
      this.register();
    }
  }
  register() {
    console.log("registering");
    const user = {
      name: this.state.data.name,
      email: this.state.data.email,
      password: this.state.data.password,
      companyName: this.state.data.companyName
    };
    this.props.register(user);
  }

  render() {
    const {
      name,
      email,
      password,
      passwordConfirmation,
      companyName,
      errors
    } = this.state;
    const { userResponse } = this.props;
    return (
      <Grid textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }} textAlign="left">
          <Header as="h1" icon color="orange" textAlign="center">
            <Icon name="puzzle piece" color="orange" />
            Register for Zervant
          </Header>
          <Form error size="large" onSubmit={this.handleSubmit}>
            <Segment stacked>
              <Form.Field>
                <Form.Input
                  fluid
                  name="name"
                  icon="user"
                  iconPosition="left"
                  placeholder="Username"
                  onChange={this.handleChange}
                  value={name}
                  type="text"
                />
                {errors["name"] && (
                  <Message
                    style={{ marginTop: "-1em" }}
                    error
                    content={errors["name"]}
                  />
                )}
              </Form.Field>
              <Form.Field>
                <Form.Input
                  fluid
                  name="email"
                  icon="mail"
                  iconPosition="left"
                  placeholder="Email Address"
                  onChange={this.handleChange}
                  value={email}
                  type="email"
                />
                {(errors["email"] || userResponse) && (
                  <Message
                    style={{ marginTop: "-1em" }}
                    error
                    content={errors["email"] || userResponse}
                  />
                )}
              </Form.Field>
              <Form.Field>
                <Form.Input
                  fluid
                  name="password"
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  onChange={this.handleChange}
                  value={password}
                  type="password"
                />
                {errors["password"] && (
                  <Message
                    style={{ marginTop: "-1em" }}
                    error
                    content={errors["password"]}
                  />
                )}
              </Form.Field>
              <Form.Field>
                <Form.Input
                  fluid
                  name="passwordConfirmation"
                  icon="repeat"
                  iconPosition="left"
                  placeholder="Password Confirmation"
                  onChange={this.handleChange}
                  value={passwordConfirmation}
                  type="password"
                />
                {errors["passwordConfirmation"] && (
                  <Message
                    style={{ marginTop: "-1em" }}
                    error
                    content={errors["passwordConfirmation"]}
                  />
                )}
              </Form.Field>
              <Form.Field>
                <Form.Input
                  fluid
                  name="companyName"
                  icon="building"
                  iconPosition="left"
                  placeholder="Your Company"
                  onChange={this.handleChange}
                  value={companyName}
                  type="text"
                />
                {errors["companyName"] && (
                  <Message
                    style={{ marginTop: "-1em" }}
                    error
                    content={errors["companyName"]}
                  />
                )}
              </Form.Field>

              <Button color="orange" fluid size="large">
                Submit
              </Button>
              <Message>
                Already a user? <Link to="/login">Login</Link>
              </Message>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = ({ locale, users }) => {
  const { userResponse } = users;
  return { locale, userResponse };
};

export default connect(
  mapStateToProps,
  { register }
)(Register);
