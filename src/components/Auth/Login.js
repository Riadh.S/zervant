import React from "react";
import Joi from "joi";
import JoiForm from "../../commons/Form";
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Message,
  Icon
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { login, logout } from "../../_actions";
import { FormattedMessage } from "react-intl";
import message from "../../translations";
import { withRouter } from "react-router-dom";

export class Login extends JoiForm {
  state = {
    data: {
      email: "",
      password: ""
    },
    errors: "",
    loading: false
  };

  schema = {
    email: Joi.string()
      .email()
      .required()
      .error(() => message[this.props.locale.lang]["error-message-email"]),
    password: Joi.string()
      .required()
      .min(6)
      .error(() => message[this.props.locale.lang]["error-message-password"])
  };

  doSubmit() {
    this.login();
  }

  login = () => {
    const { email, password } = this.state.data;
    if (email && password) {
      this.props.login(email, password);
    }
  };

  render() {
    return (
      <Grid textAlign="center" verticalAlign="middle" className="app">
        <Grid.Column style={{ maxWidth: 450 }} textAlign="left">
          <Header as="h1" icon color="violet" textAlign="center">
            <Icon name="code branch" color="violet" />
            Login to Zervant
          </Header>
          <Form error size="large" onSubmit={this.handleSubmit}>
            <Segment stacked>
              {this.inputForm(
                "Email Address",
                "email",
                "user@example.com",
                "email",
                false,
                "mail",
                "left"
              )}
              {this.inputForm(
                "Password",
                "password",
                "****",
                "password",
                false,
                "lock",
                "left"
              )}

              {/* <Form.Input
                fluid
                name="email"
                icon="mail"
                iconPosition="left"
                placeholders="Email Address"
                onChange={this.handleChange}
                value={email}
                type="email"
              />
              <Form.Input
                fluid
                name="password"
                icon="lock"
                iconPosition="left"
                placeholders="Password"
                onChange={this.handleChange}
                value={password}
                type="password"
              /> */}

              <Button color="violet" fluid size="large">
                Submit
              </Button>

              <Message>
                Don't have an account ? <Link to="/register">Register</Link>
              </Message>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = ({ locale }) => {
  return { locale };
};
export default withRouter(
  connect(
    mapStateToProps,
    { login, logout }
  )(Login)
);
