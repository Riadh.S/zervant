import React, { Component } from "react";
import {
  Grid,
  Table,
  Menu,
  Icon,
  Label,
  Segment,
  Header,
  Input,
  Button,
  Sidebar,
  Message,
  Divider,
  Modal,
  Flag,
  Form
} from "semantic-ui-react";
import CustomerForm from "./CustomerFrom";
import { getCustomers, deleteCustomer } from "../../_actions/customerAction";
import { connect } from "react-redux";
import { paginate } from "../../utils/paginate";
import Pagination from "../../commons/Pagination";
import _ from "lodash";

export class Customers extends Component {
  state = {
    visible: true,
    modal: false,
    searchTerm: "",
    currentPage: 1,
    pageSize: 4,
    sortColumn: { column: "name", order: "asc" },
    customer: "",
    deleteConfirm: false,
    edit: false
  };

  componentWillMount() {
    this.props.getCustomers();
  }

  openModal = () => {
    this.setState({ modal: true });
  };

  closeModal = () => {
    this.setState({ modal: false, edit: false });
  };

  openEditModal = () => this.setState({ modal: true, edit: true });

  handleAnimationChange = () => () =>
    this.setState({ visible: !this.state.visible });

  handleSearchChange = e =>
    this.setState({ [e.target.name]: e.target.value, currentPage: 1 });

  handlePageChange = page => this.setState({ currentPage: page });

  handleCustomerDetails = customer => this.setState({ customer });

  handleSort = column => {
    var sortColumn = { column };
    if (column === this.state.sortColumn.column)
      sortColumn.order = this.state.sortColumn.order === "asc" ? "desc" : "asc";
    else {
      sortColumn.order = "asc";
    }
    this.setState({ sortColumn, currentPage: 1 });
  };

  renderSortIcon = column => {
    const { sortColumn } = this.state;

    if (column !== sortColumn.column) return <Icon name="sort" />;
    if (sortColumn.order === "asc") return <Icon name="sort up" />;
    return <Icon name="sort down" />;
  };

  openDeleteModal = () => this.setState({ deleteConfirm: true });
  closeDeleteModal = () => this.setState({ deleteConfirm: false });

  handleDelete = id => {
    this.props.deleteCustomer(id);
    this.closeDeleteModal();
    this.props.getCustomers();
  };

  render() {
    const {
      visible,
      searchTerm,
      currentPage,
      pageSize,
      sortColumn,
      customer,
      deleteConfirm,
      edit
    } = this.state;

    const { customers: allCustomers } = this.props;

    const customersData =
      searchTerm !== ""
        ? allCustomers.filter(customer => customer.name.startsWith(searchTerm))
        : allCustomers;
    const totalCount = customersData.length;

    const sortedCustomers = _.orderBy(
      customersData,
      [sortColumn.column],
      [sortColumn.order]
    );
    const customers = paginate(sortedCustomers, currentPage, pageSize);
    console.log(allCustomers);
    return (
      <Grid divided="vertically">
        <Grid.Row columns={1}>
          <Grid.Column>
            <Segment clearing>
              <Header
                fluid="true"
                as="h2"
                floated="left"
                style={{ marginBottom: 0 }}
              >
                <span>
                  Customers
                  <Icon name="product hunt" color="black" />
                </span>
                <Header.Subheader>
                  {allCustomers.length} Customers
                </Header.Subheader>
              </Header>
              <Header floated="right">
                <Input
                  // loading={searchLoading}
                  onChange={this.handleSearchChange}
                  size="mini"
                  icon="search"
                  name="searchTerm"
                  placeholder="Search Messages"
                />
              </Header>
              <Button.Group>
                <Button onClick={this.handleAnimationChange()}>
                  Show sidebar
                </Button>
                <Button onClick={this.openModal}>New Customer</Button>
                <Button onClick={this.handleAnimationChange()}>Export</Button>
              </Button.Group>
            </Segment>

            <Sidebar.Pushable style={{ height: "80vh" }}>
              <Sidebar
                as={Grid}
                direction="right"
                animation="overlay"
                icon="labeled"
                vertical
                visible={visible}
                style={{ width: "50%", paddingRight: 0 }}
              >
                <Grid
                  style={{
                    background: "#fff",
                    width: "100%",
                    marginLeft: 0
                  }}
                  textAlign="center"
                  verticalAlign="middle"
                >
                  <Grid.Column fluid>
                    {!customer && (
                      <Icon
                        name="file alternate outline"
                        size="huge"
                        bordered
                      />
                    )}
                    {customer && (
                      <React.Fragment>
                        <Icon name="user" size="big" color="black" circular />
                        <Header as="h1" textAlign="center">
                          {`${customer.firstName}  ${customer.lastName}`}
                        </Header>
                        <Header sub>
                          Type Of Customer :
                          <span> {customer.typeOfCustomer}</span>
                        </Header>
                        <Header sub>
                          Company Name : <span> {customer.companyName}</span>
                        </Header>
                        <Header sub>
                          Company Registration Number :
                          <span> {customer.companyRegistrationNumber}</span>
                        </Header>
                        <Header sub>
                          VAT Number : <span> {customer.vatNumber}</span>
                        </Header>
                        <Header sub>
                          Title : <span> {customer.title}</span>
                        </Header>
                        <Header sub>
                          Email Customer :<span> {customer.emailCustomer}</span>
                        </Header>
                        <Header sub>
                          Phone : <span> {customer.phone}</span>
                        </Header>
                        <Header sub>
                          Mobile Phone : <span> {customer.mobilePhone}</span>
                        </Header>
                        <Header sub>
                          Address : <span> {customer.address}</span>
                        </Header>
                        <Header sub>
                          Post Code : <span> {customer.postCode}</span>
                        </Header>
                        <Header sub>
                          City : <span> {customer.city}</span>
                        </Header>
                        <Header sub>
                          Country : <span> {customer.country}</span>
                        </Header>
                        <Header sub>
                          Invoicing Language :
                          <span> {customer.invoicingLanguage}</span>
                        </Header>
                        <Header sub>
                          Payment Term : <span> {customer.paymentTerm}</span>
                        </Header>

                        <Header sub>
                          Creation date : <span> {customer.createdAt}</span>
                        </Header>
                      </React.Fragment>
                    )}

                    {customer && (
                      <React.Fragment>
                        <Divider section />
                        <Button
                          onClick={this.openEditModal}
                          className="editButton"
                        >
                          <Icon name="edit" /> Edit
                        </Button>
                        <Button
                          className="deleteButton"
                          onClick={this.openDeleteModal}
                        >
                          <Icon name="delete" /> Delete
                        </Button>
                      </React.Fragment>
                    )}
                  </Grid.Column>
                </Grid>
              </Sidebar>

              <Sidebar.Pusher>
                <Table celled style={{ width: visible ? "50%" : "100%" }}>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("typeOfCustomer")}
                        >
                          Type Of Customer
                          {this.renderSortIcon("typeOfCustomer")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("companyName")}
                        >
                          Company Name {this.renderSortIcon("companyName")}
                        </label>
                      </Table.HeaderCell>
                      {/* {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() =>
                              this.handleSort("companyRegistrationNumber")
                            }
                          >
                            Company Registration Number
                            {this.renderSortIcon("companyRegistrationNumber")}
                          </label>
                        </Table.HeaderCell>
                      )} 
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("vatNumber")}
                          >
                            VAT Number {this.renderSortIcon("vatNumber")}
                          </label>
                        </Table.HeaderCell>
                      )}*/}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("title")}
                          >
                            Title {this.renderSortIcon("title")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("firstName")}
                        >
                          First Name {this.renderSortIcon("firstName")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("lastName")}
                        >
                          Last Name{this.renderSortIcon("lastName")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("emailCustomer")}
                        >
                          Email Customer
                          {this.renderSortIcon("emailCustomer")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("phone")}
                        >
                          Phone
                          {this.renderSortIcon("phone")}
                        </label>
                      </Table.HeaderCell>
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("mobilePhone")}
                          >
                            Mobile Phone
                            {this.renderSortIcon("mobilePhone")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {/* {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("address")}
                          >
                            Address
                            {this.renderSortIcon("address")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("postCode")}
                          >
                            Post Code
                            {this.renderSortIcon("postCode")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("city")}
                          >
                            City
                            {this.renderSortIcon("city")}
                          </label>
                        </Table.HeaderCell>
                      )} */}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("country")}
                          >
                            Country
                            {this.renderSortIcon("country")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("invoicingLanguage")}
                          >
                            Invoicing Language
                            {this.renderSortIcon("invoicingLanguage")}
                          </label>
                        </Table.HeaderCell>
                      )}
                      {!visible && (
                        <Table.HeaderCell>
                          <label
                            className="pointer"
                            onClick={() => this.handleSort("paymentTerm")}
                          >
                            Payment Term
                            {this.renderSortIcon("paymentTerm")}
                          </label>
                        </Table.HeaderCell>
                      )}
                    </Table.Row>
                  </Table.Header>

                  <Table.Body>
                    {customers.map(customer => (
                      <Table.Row
                        key={customer._id}
                        onClick={() => this.handleCustomerDetails(customer)}
                      >
                        <Table.Cell>{customer.typeOfCustomer}</Table.Cell>
                        <Table.Cell>{customer.companyName}</Table.Cell>
                        {!visible && <Table.Cell>{customer.title}</Table.Cell>}
                        <Table.Cell>{customer.firstName}</Table.Cell>
                        <Table.Cell>{customer.lastName}</Table.Cell>
                        <Table.Cell>{customer.emailCustomer}</Table.Cell>
                        <Table.Cell>{customer.phone}</Table.Cell>
                        {!visible && (
                          <Table.Cell>{customer.mobilePhone}</Table.Cell>
                        )}
                        {!visible && (
                          <Table.Cell>
                            {customer.country}
                            {"    "}
                            <Flag name={customer.country} />
                          </Table.Cell>
                        )}
                        {!visible && (
                          <Table.Cell>{customer.invoicingLanguage}</Table.Cell>
                        )}
                        {!visible && (
                          <Table.Cell>{customer.paymentTerm}%</Table.Cell>
                        )}
                      </Table.Row>
                    ))}
                  </Table.Body>

                  <Table.Footer>
                    <Table.Row>
                      <Table.HeaderCell colSpan={visible ? 6 : 11}>
                        <Pagination
                          itemsCount={totalCount}
                          pageSize={pageSize}
                          currentPage={currentPage}
                          onPageChange={this.handlePageChange}
                        />
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Footer>
                </Table>
              </Sidebar.Pusher>
            </Sidebar.Pushable>
            <CustomerForm
              show={this.state.modal}
              closeModal={this.closeModal}
              customer={edit ? customer : null}
              getCustomers={this.props.getCustomers}
            />
            <Modal
              size="tiny"
              basic
              open={deleteConfirm}
              onClose={this.closeDeleteModal}
            >
              <Modal.Header as="h1" icon color="orange" textAlign="center">
                <Icon name="dolly flatbed" color="orange" />
                {"    "}
                Confirmation
              </Modal.Header>
              <Modal.Content>
                <Segment stacked textAlign="left" style={{ padding: "3em" }}>
                  <Header sub>
                    Are you sure you want to delete this customer ?
                  </Header>
                  <span>$10.99</span>
                  <Divider hidden />
                </Segment>
                <Button
                  floated="right"
                  className="deleteButton-inverted"
                  inverted
                  size="large"
                  onClick={() => this.handleDelete(customer._id)}
                >
                  <Icon name="remove" /> Delete
                </Button>
                <Button
                  floated="right"
                  color="grey"
                  inverted
                  size="large"
                  onClick={this.closeDeleteModal}
                >
                  <Icon name="dont" /> Cancel
                </Button>
              </Modal.Content>
              <Modal.Actions />
            </Modal>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = ({ customer }) => {
  const { customersList } = customer;
  return { customers: customersList };
};

export default connect(
  mapStateToProps,
  { getCustomers, deleteCustomer }
)(Customers);
