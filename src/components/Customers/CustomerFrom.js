import React, { Component } from "react";
import {
  Modal,
  Form,
  Grid,
  Segment,
  Button,
  Icon,
  Dropdown,
  Label,
  Message,
  Radio,
  Divider,
  Header
} from "semantic-ui-react";
import { connect } from "react-redux";
import JoiForm from "../../commons/Form";
import { calculateTTC } from "../../_services/GlobalFunctions";
import { FormattedMessage } from "react-intl";
import Joi from "joi";
import message from "../../translations";
import { getIdUser } from "../../_services/UserServices";
import { addCustomer, updateCustomer } from "../../_actions/customerAction";
import _ from "lodash";
import { countryOptions } from "../../commons/Flags";
import {
  typeOfCustomer,
  invoicingLanguage,
  paymentTerm
} from "../../config/customerData";

export class CustomerForm extends JoiForm {
  schema = {
    typeOfCustomer: Joi.string(),
    companyName: Joi.string()
      .required()
      .error(() => message[this.props.lang]["error-message-CompanyName"]),
    companyRegistrationNumber: Joi.string().error(
      () => message[this.props.lang]["error-message-CompanyRegistrationNumber"]
    ),
    vatNumber: Joi.string().error(
      () => message[this.props.lang]["error-message-VatNumber"]
    ),
    title: Joi.string().error(
      () => message[this.props.lang]["error-message-title"]
    ),
    firstName: Joi.string().error(
      () => message[this.props.lang]["error-message-firstName"]
    ),
    lastName: Joi.string().error(
      () => message[this.props.lang]["error-message-lastName"]
    ),
    emailCustomer: Joi.string()
      .email()
      .error(() => message[this.props.lang]["error-message-emailCustomer"]),
    phone: Joi.string().error(
      () => message[this.props.lang]["error-message-phone"]
    ),
    mobilePhone: Joi.string().error(
      () => message[this.props.lang]["error-message-mobilePhone"]
    ),
    address: Joi.string().error(
      () => message[this.props.lang]["error-message-address"]
    ),
    postCode: Joi.string().error(
      () => message[this.props.lang]["error-message-postCode"]
    ),
    city: Joi.string(),
    country: Joi.string().error(
      () => message[this.props.lang]["error-message-country"]
    ),
    invoicingLanguage: Joi.string(),
    paymentTerm: Joi.string(),

    deliveryMethod: Joi.string().required()
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.customer) {
      let data = { ...nextProps.customer };
      const customerId = data._id;
      data = _.omit(data, ["__v", "createdAt", "id_user", "updatedAt", "_id"]);
      this.setState({ data, customerId });
    } else {
      const data = {};
      const errors = {};
      const customerId = null;
      this.setState({ data, errors, customerId });
    }
  }

  doSubmit() {
    const { data, customerId } = this.state;
    let customer = {
      ...data,
      id_user: getIdUser()
    };
    if (!this.props.customer) this.props.addCustomer(customer);
    else this.props.updateCustomer(customerId, customer);
    this.props.getCustomers();
    this.closeModal();
  }

  closeModal = () => {
    this.props.closeModal();
    this.setState({ data: {} });
  };

  render() {
    const { errors, data } = this.state;
    const { customer } = this.props;

    return (
      <Modal
        size="small"
        basic
        open={this.props.show}
        onClose={this.closeModal}
      >
        <Modal.Header as="h1" icon color="orange" textAlign="center">
          <Icon name="dolly flatbed" color="orange" />
          {"    "}
          {customer ? "Edit Customer" : "Add a Customer"}
        </Modal.Header>
        <Modal.Content>
          <Grid textAlign="center" verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 800 }} textAlign="left">
              <Form error size="large" onSubmit={this.handleSubmit}>
                <Segment stacked textAlign="left" style={{ padding: "3em" }}>
                  <Form.Group inline fluid>
                    <label>Delivery Method:</label>
                    <Form.Radio
                      label="By Email"
                      name="deliveryMethod"
                      value="byEmail"
                      checked={data.deliveryMethod === "byEmail"}
                      onChange={this.customHandleChange}
                    />

                    <Form.Radio
                      label="By Post"
                      name="deliveryMethod"
                      value="byPost"
                      checked={data.deliveryMethod === "byPost"}
                      onChange={this.customHandleChange}
                    />

                    <Form.Radio
                      label="None"
                      name="deliveryMethod"
                      value="none"
                      checked={data.deliveryMethod === "none"}
                      onChange={this.customHandleChange}
                    />
                  </Form.Group>
                  {errors["deliveryMethod"] && (
                    <Message
                      //style={{ marginTop: "-1em" }}
                      error
                      content={errors["deliveryMethod"]}
                    />
                  )}
                  <Divider hidden />
                  <Grid columns={2}>
                    <Grid.Column>
                      <Header size="medium">Customer Details</Header>
                      <Form.Field>
                        <label>Type Of Customer</label>
                        <Dropdown
                          name="typeOfCustomer"
                          placeholder="Type Of Customer"
                          fluid
                          selection
                          options={typeOfCustomer}
                          onChange={this.customHandleChange}
                          defaultValue={
                            data.typeOfCustomer && data.typeOfCustomer
                          }
                        />
                      </Form.Field>
                      {this.inputForm("Company Name", "companyName")}
                      {this.inputForm(
                        "Company Registration Number",
                        "companyRegistrationNumber"
                      )}
                      {this.inputForm("VAT Number", "vatNumber")}
                      {this.inputForm("Title", "title")}
                      {this.inputForm("First Name", "firstName")}
                      {this.inputForm("Last Name", "lastName")}
                      {this.inputForm("Email Customer", "emailCustomer")}
                      {this.inputForm("Phone", "phone")}
                      {this.inputForm("Mobile Phone", "mobilePhone")}
                    </Grid.Column>
                    <Grid.Column>
                      <Header size="medium">Invoicing address</Header>
                      {this.inputForm("Address", "address")}
                      {this.inputForm("Post Code", "postCode")}
                      {this.inputForm("City", "city")}
                      <Form.Field>
                        <label>Country</label>
                        <Dropdown
                          name="country"
                          placeholder="Country"
                          fluid
                          search
                          selection
                          options={countryOptions}
                          onChange={this.customHandleChange}
                          defaultValue={data.country && data.country}
                        />
                      </Form.Field>

                      <Header size="medium">Invoicing Details</Header>
                      <Form.Field>
                        <label>Invoicing Language</label>
                        <Dropdown
                          name="invoicingLanguage"
                          placeholder="Invoicing Language"
                          fluid
                          selection
                          options={invoicingLanguage}
                          onChange={this.customHandleChange}
                          defaultValue={
                            data.invoicingLanguage && data.invoicingLanguage
                          }
                        />
                      </Form.Field>
                      <Form.Field>
                        <label>Payment Term</label>
                        <Dropdown
                          name="paymentTerm"
                          placeholder="Payment Term"
                          fluid
                          selection
                          options={paymentTerm}
                          onChange={this.customHandleChange}
                          defaultValue={data.paymentTerm && data.paymentTerm}
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid>
                </Segment>

                {this.buttonSubmit("Submit")}
                <Button
                  floated="right"
                  color="red"
                  inverted
                  size="large"
                  onClick={this.closeModal}
                >
                  <Icon name="remove" /> Cancel
                </Button>
              </Form>
            </Grid.Column>
          </Grid>
        </Modal.Content>
        <Modal.Actions />
      </Modal>
    );
  }
}

const mapStateToProps = ({ locale }) => {
  const { lang } = locale;
  return { lang };
};

export default connect(
  mapStateToProps,
  { addCustomer, updateCustomer }
)(CustomerForm);
