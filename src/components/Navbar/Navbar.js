import React, { Component } from "react";
import { Menu, Dropdown, Flag, Icon } from "semantic-ui-react";
import UserPanel from "./UserPanel";
import { Link, withRouter } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { logout, userActions, setLocale } from "../../_actions";
export class Navbar extends Component {
  state = { activeItem: "home" };

  logout = () => {
    this.props.logout();
  };

  changeLanguage = lang => {
    console.log("lang", lang);
    //  this.props.setLocale(lang);
  };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  onChange = (e, { value }) => this.props.setLocale(value);

  render() {
    const { activeItem } = this.state;
    return (
      <Menu
        size="large"
        inverted
        fixed="left"
        vertical
        style={{ background: "#4c3c4c", fontSize: "1.2rem" }}
        secondary
      >
        <UserPanel />
        <Menu.Item
          as={Link}
          to="home"
          name="home"
          active={activeItem === "home"}
          onClick={this.handleItemClick}
        >
          <Icon name="home" />

          <FormattedMessage id={"nav-Home"} defaultMessage={"Home"} />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="products"
          name="products"
          active={activeItem === "products"}
          onClick={this.handleItemClick}
        >
          <Icon name="product hunt" />
          <FormattedMessage id={"nav-Products"} defaultMessage={"Products"} />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="customers"
          name="customers"
          active={activeItem === "customers"}
          onClick={this.handleItemClick}
        >
          <Icon name="group" />
          <FormattedMessage id={"nav-Customers"} defaultMessage={"Customers"} />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="invoices"
          name="invoices"
          active={activeItem === "invoices"}
          onClick={this.handleItemClick}
        >
          <Icon name="file text" />
          <FormattedMessage id={"nav-Invoice"} defaultMessage={"Invoices"} />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="estimation"
          name="estimation"
          active={activeItem === "estimation"}
          onClick={this.handleItemClick}
        >
          <Icon name="file" />
          <FormattedMessage
            id={"nav-Estimation"}
            defaultMessage={"Estimation"}
          />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="report"
          name="report"
          active={activeItem === "report"}
          onClick={this.handleItemClick}
        >
          <Icon name="chart line" />
          <FormattedMessage id={"nav-Report"} defaultMessage={"Report"} />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="technicalSupport"
          name="technicalSupport"
          active={activeItem === "technicalSupport"}
          onClick={this.handleItemClick}
        >
          <Icon name="tasks" />
          <FormattedMessage id={"nav-Support"} defaultMessage={"Support"} />
        </Menu.Item>

        <Menu.Item
          as={Link}
          to="account"
          name="account"
          active={activeItem === "account"}
          onClick={this.handleItemClick}
        >
          <Icon name="user" />

          <FormattedMessage id={"nav-account"} defaultMessage={"My account"} />
        </Menu.Item>

        {/* <Menu.Item
          name="logout"
          active={activeItem === "logout"}
          onClick={() => this.logout()}
        >
          <FormattedMessage id={"nav-Logout"} defaultMessage={"Logout"} />
        </Menu.Item> */}
        <Dropdown
          style={{ marginTop: "1rem" }}
          button
          onChange={this.onChange}
          options={languageOptions}
          floating
          trigger={languageOptions
            .filter(lang => lang.value === this.props.lang)
            .map(item => (
              <span key="defaultLang">
                <Flag name={item.flag} /> {item.text}
              </span>
            ))}
        />
      </Menu>
    );
  }
}
const mapStateToProps = ({ authentication, locale, userAccount }) => {
  const { loggingIn } = authentication;
  const { lang } = locale;
  const { data } = userAccount;
  return {
    loggingIn,
    lang,
    data
  };
};

const languageOptions = [
  {
    key: "English",
    text: <span key="en">English</span>,
    value: "en",
    flag: "us"
  },
  {
    key: "German",
    text: <span key="de">German</span>,
    value: "de",
    flag: "de"
  }
];

export default withRouter(
  connect(
    mapStateToProps,
    { logout, userActions, setLocale }
  )(Navbar)
);
