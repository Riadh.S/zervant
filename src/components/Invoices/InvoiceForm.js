import React, { Component } from "react";
import {
  Grid,
  Segment,
  Header,
  Input,
  Button,
  Dropdown,
  Form,
  Divider,
  Label,
  List,
  TextArea,
  Table,
  Icon,
  Flag,
  Menu
} from "semantic-ui-react";
import JoiForm from "../../commons/Form";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Item from "./Item";
import _ from "lodash";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const customerOptions = [
  { key: "Client s.a.r.l", text: "Client s.a.r.l", value: "Client s.a.r.l" },
  {
    key: "Client s.u.a.r.l",
    text: "Client s.u.a.r.l",
    value: "Client s.u.a.r.l"
  }
];

const invoiceTypes = [
  { key: "sales", text: "sales", value: "sales" },
  {
    key: "salesExcludingVat",
    text: "salesExcludingVat",
    value: "salesExcludingVat"
  }
];

const invoiceColumns = [
  { key: "date", text: "Date", value: "date" },

  {
    key: "qty",
    text: "Qty",
    value: "qty"
  },
  {
    key: "unit",
    text: "Unit",
    value: "unit"
  },
  {
    key: "unitPrice",
    text: "Unit Price",
    value: "unitPrice"
  },
  {
    key: "vatPercent",
    text: "VAT %",
    value: "vatPercent"
  },
  {
    key: "vatTotal",
    text: "Vat Total",
    value: "vatTotal"
  }
];

const invoicingLanguages = [
  { key: "english", text: "English", value: "en", flag: "us" },
  { key: "german", text: "German", value: "de", flag: "de" }
];

export class InvoiceForm extends JoiForm {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        quantity: "",
        customer: "",
        invoiceDate: new Date(),
        dueDate: new Date(),
        message: "",
        itemsList: [],
        footnote: "",
        selectedColumns: ["date", "qty", "unitPrice", "vatTotal"],
        invoicingLanguage: this.props.lang
      },
      errors: "",
      itemsError: false,
      itemsNumber: 1,
      validItemsCount: 0,
      submitItems: false,
      currentStep: 1
    };
  }

  handleChangeDate = invoiceDate => {
    this.setState({ invoiceDate });
  };

  handleChangeDueDate = dueDate => {
    this.setState({ dueDate });
  };

  handleChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  handleItemsRows = action => {
    const itemsNumber =
      action === "add"
        ? this.state.itemsNumber + 1
        : this.state.itemsNumber - 1;
    this.setState({ itemsNumber });
  };

  itemSubmit = (state, errors, id) => {
    if (_.isEmpty(errors)) {
      let found = this.state.data.itemsList.find(
        itemList => id === itemList.id
      );
      if (found) console.log("found", found);
      else var itemsList = this.state.data.itemsList.push({ ...state, id });
      this.setState({ itemsList, submitItems: false, itemsError: false });
    } else this.setState({ itemsError: true });
  };

  handleSubmitClicked = () => {
    this.setState({ submitItems: true });
  };

  handleColumnChange = (e, event) => {
    const data = { ...this.state.data };
    data[event.name] = event.value;
    this.setState({ data });
  };

  render() {
    const { itemsNumber, submitItems, data, columns } = this.state;
    console.log("this.state.currentStep", this.state.currentStep);
    return (
      <React.Fragment>
        <Segment className="invoice header">
          <Menu secondary>
            <Menu.Item name="home">
              <Header color="blue" size="small" className="invoiceSteps active">
                <span id="stepOne" className="step-number">{` 1 `}</span>
                <span>New Invoice</span>
              </Header>
            </Menu.Item>

            <Menu.Item name="messages">
              <Header
                size="small"
                className={`invoiceSteps " ${
                  this.state.currentStep === 2 ? "active" : ""
                }`}
              >
                <span style={{ fontSize: "2rem", marginTop: "2em" }}>></span>
                <span id="stepTwo" className="step-number">{` 2 `}</span>
                <span>Approve and send</span>
              </Header>
            </Menu.Item>
          </Menu>
        </Segment>
        <Grid divided="vertically" style={{ padding: "2em" }} relaxed>
          <Grid.Column width={13}>
            <Segment clearing style={{ width: "100%" }}>
              <Form fluid error>
                <Grid.Row>
                  <Segment clearing padded basic>
                    <Header fluid="true" as="h2" floated="left">
                      <span>Customer</span>
                    </Header>
                    <Dropdown
                      name="customer"
                      clearable
                      search
                      floating
                      options={customerOptions}
                      placeholder="Select a customer"
                      onChange={this.customHandleChange}
                    />

                    <Header as="h1" floated="right">
                      Facture
                    </Header>
                  </Segment>
                </Grid.Row>
                <Divider />
                <Grid columns={2} divided>
                  <Grid.Row stretched>
                    <Grid.Column>
                      <Grid columns={2}>
                        <Grid.Column>
                          <List>
                            <List.Item>
                              <Label size="large">Invoice number</Label>
                            </List.Item>
                            <List.Item>
                              <Label size="large">Date of invoice</Label>
                            </List.Item>
                            <List.Item>
                              <Label size="large">Due date</Label>
                            </List.Item>
                          </List>
                        </Grid.Column>
                        <Grid.Column divided>
                          <Input placeholder="Invoice numbe" />
                          <DatePicker
                            selected={this.state.invoiceDate}
                            onChange={this.handleChangeDate}
                            dateFormat="dd/MM/yyyy"
                          />
                          <DatePicker
                            selected={this.state.dueDate}
                            onChange={this.handleChangeDueDate}
                            dateFormat="dd/MM/yyyy"
                          />
                        </Grid.Column>
                      </Grid>
                    </Grid.Column>
                    <Grid.Column>
                      <Segment basic>
                        <Label size="large">
                          Message (
                          {this.state.message ? this.state.message.length : 0} /
                          3000)
                        </Label>
                        <TextArea
                          name="message"
                          value={this.state.message}
                          onChange={this.handleChange}
                        />
                      </Segment>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Divider />
                <Grid padded>
                  <Grid.Row>
                    <Table fixed>
                      <Table.Header>
                        <Table.Row>
                          <Table.HeaderCell>Product</Table.HeaderCell>
                          {data.selectedColumns.map(item => (
                            <Table.HeaderCell>
                              {_.find(invoiceColumns, { key: item }).text}
                            </Table.HeaderCell>
                          ))}
                          <Table.HeaderCell>Total</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>

                      <Table.Body>
                        {_.range(0, this.state.itemsNumber).map(id => (
                          <Item
                            id={id}
                            itemSubmit={this.itemSubmit}
                            submitItems={submitItems}
                            selectedColumns={data.selectedColumns}
                          />
                        ))}
                      </Table.Body>
                    </Table>
                  </Grid.Row>
                  <Button
                    color="teal"
                    circular
                    onClick={() => this.handleItemsRows("add")}
                  >
                    <Icon name="add" />
                    Add Item
                  </Button>
                  <Button secondary circular onClick={this.handleSubmitClicked}>
                    submit
                  </Button>
                </Grid>
                <Divider hidden />
                <Segment basic style={{ width: "50%" }}>
                  <Label size="large">
                    Invoice footnote (
                    {this.state.footnote ? this.state.footnote.length : 0} /
                    250)
                  </Label>
                  <TextArea
                    name="footnote"
                    value={this.state.footnote}
                    onChange={this.handleChange}
                  />
                </Segment>
              </Form>
            </Segment>
          </Grid.Column>
          <Grid.Column width={3}>
            <Segment
              verticalAlign="center"
              textAlign="left"
              style={{ paddingLeft: "1.5em" }}
            >
              <Header color="grey" size="small" style={{ marginTop: "1em" }}>
                INVOICE SETTINGS
              </Header>
              <Divider />
              <Header color="grey" size="tiny">
                Company Name
              </Header>
              <p color="black">licht und schatten</p>
              <Link
                to="/account"
                style={{ fontSize: "1rem", fontWeight: "600" }}
              >
                Edit Company Details
              </Link>
              <Divider />
              <Header color="grey" size="small">
                INVOICE TYPE
              </Header>
              <Dropdown
                name="invoiceTypes"
                floating
                options={invoiceTypes}
                onChange={this.customHandleChange}
                defaultValue={invoiceTypes[0].value}
              />
              <Divider />
              <Header color="grey" size="small">
                VISIBLE COLUMNS
              </Header>
              <Dropdown
                name="selectedColumns"
                multiple
                selection
                options={invoiceColumns}
                onChange={this.handleColumnChange}
                defaultValue={this.state.data.selectedColumns}
                className="invoice visibleColumn"
              />
              <Divider />
              <Header color="grey" size="small">
                INVOICING LANGUAGE
              </Header>
              <Dropdown
                name="invoicingLanguage"
                selection
                options={invoicingLanguages}
                onChange={this.customHandleChange}
                defaultValue={this.state.data.invoicingLanguage}
                trigger={invoicingLanguages
                  .filter(
                    lang => lang.value === this.state.data.invoicingLanguage
                  )
                  .map(item => (
                    <span key="defaultLang">
                      <Flag name={item.flag} /> {item.text}
                    </span>
                  ))}
              />
            </Segment>
          </Grid.Column>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ locale }) => {
  const { lang } = locale;
  return {
    lang
  };
};

export default connect(
  mapStateToProps,
  {}
)(InvoiceForm);
