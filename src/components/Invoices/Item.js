import React, { Component } from "react";
import { Table, Dropdown, Button, Message } from "semantic-ui-react";
import JoiForm from "../../commons/Form";
import Joi from "joi";
import DatePicker from "react-datepicker";
import { changeFormatDate } from "../../_services/GlobalFunctions";
import _ from "lodash";

const products = [
  { key: "product 1", text: "product 1", value: "product 1" },
  {
    key: "product 2",
    text: "product 2",
    value: "product 2"
  },
  {
    key: "product 3",
    text: "product 3",
    value: "product 3"
  }
];

export class Item extends JoiForm {
  constructor(props) {
    super(props);
    this.state = {
      data: { date: new Date() },
      errors: {},
      id: this.props.id
    };
  }

  changeSchema = nextProps => {
    const schema = {
      product: Joi.string().required(),
      date: nextProps.selectedColumns.includes("date")
        ? Joi.date().required()
        : Joi.date().optional(),
      qty: nextProps.selectedColumns.includes("qty")
        ? Joi.number().required()
        : Joi.number().optional(),
      unitPrice: nextProps.selectedColumns.includes("unitPrice")
        ? Joi.number().required()
        : Joi.number().optional(),
      vat: nextProps.selectedColumns.includes("vat")
        ? Joi.number().required()
        : Joi.number().optional()
    };
    return schema;
  };

  changeColumns = () => {
    const columns = {
      date: this.props.selectedColumns.includes("date") ? (
        <Table.Cell>
          <DatePicker
            selected={this.state.data.date}
            onChange={this.handleChangeItemDate}
            dateFormat="dd/MM/yyyy"
          />
          {this.state.errors["date"] && (
            <Message
              style={{ marginTop: "0em" }}
              error
              content={this.state.errors["date"]}
            />
          )}
        </Table.Cell>
      ) : null,

      qty: this.props.selectedColumns.includes("qty") ? (
        <Table.Cell>{this.inputForm(false, "qty")}</Table.Cell>
      ) : null,
      unit: this.props.selectedColumns.includes("unit") ? (
        <Table.Cell>
          <label>unit</label>
        </Table.Cell>
      ) : null,
      unitPrice: this.props.selectedColumns.includes("unitPrice") ? (
        <Table.Cell>{this.inputForm(false, "unitPrice")}</Table.Cell>
      ) : null,
      vat: this.props.selectedColumns.includes("vat") ? (
        <Table.Cell>{this.inputForm(false, "vat")}</Table.Cell>
      ) : null,
      vatTotal: (
        <Table.Cell>
          <label>Vat Total</label>
        </Table.Cell>
      )
    };
    return columns;
  };

  handleChangeItemDate = date => {
    const data = { ...this.state.data };
    data.date = date;
    this.setState({ data });
  };
  doSubmit = () => {
    this.props.itemSubmit(this.state.data, this.state.errors, this.state.id);
  };

  componentWillReceiveProps(nextProps) {
    this.changeSchema(nextProps);
    this.schema = this.changeSchema(nextProps);
    if (nextProps.submitItems) {
      const errors = this.validate();
      this.setState({ errors: errors || {} });

      if (_.isEmpty(errors)) this.doSubmit();
    }
  }

  render() {
    const { errors } = this.state;
    const { selectedColumns } = this.props;
    const columns = { ...this.changeColumns() };
    console.log("item", this.props);
    return (
      <Table.Row>
        <Table.Cell>
          <Dropdown
            name="product"
            placeholder="product"
            selection
            options={products}
            onChange={this.customHandleChange}
          />
          {this.state.errors["product"] && (
            <Message
              style={{ marginTop: "0em" }}
              error
              content={this.state.errors["product"]}
            />
          )}
        </Table.Cell>
        {selectedColumns.map(column => columns[column])}
        <Table.Cell>
          <label>Total</label>
        </Table.Cell>

        {/* {_.find(columns, column => column === "unit") && (
          <Table.Cell>
            <label>unit</label>
          </Table.Cell>
        )}
        {_.find(columns, column => column === "unitPrice") && (
          <Table.Cell>{this.inputForm(false, "unitPrice")}</Table.Cell>
        )}
        {_.find(columns, column => column === "vat") && (
          <Table.Cell>{this.inputForm(false, "vat")}</Table.Cell>
        )}
        {_.find(columns, column => column === "total") && (
          <Table.Cell>{this.inputForm(false, "total")}</Table.Cell>
        )} */}
      </Table.Row>
    );
  }
}

export default Item;
