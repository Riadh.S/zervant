import React, { Component } from "react";
import {
  Grid,
  Table,
  Menu,
  Icon,
  Label,
  Segment,
  Header,
  Input,
  Button,
  Sidebar,
  Message,
  Divider,
  Modal
} from "semantic-ui-react";
import { getProducts } from "../../_actions/product.actions";
import { connect } from "react-redux";
import { paginate } from "../../utils/paginate";
import Pagination from "../../commons/Pagination";
import _ from "lodash";
import { Link } from "react-router-dom";

export class Invoices extends Component {
  state = {
    visible: true,
    searchTerm: "",
    currentPage: 1,
    pageSize: 4,
    sortColumn: { column: "name", order: "asc" },
    invoice: "",
    invoices: [
      {
        _id: "00001",
        number: "TEST-1",
        cutomer: "Client s.a.r.l",
        created: "13.2.2019",
        dueDate: "15.3.2019",
        paid: "8.4.2019",
        total: "4,428",
        status: "paid"
      }
    ]
  };

  componentWillMount() {
    this.props.getProducts();
  }

  handleAnimationChange = () => () =>
    this.setState({ visible: !this.state.visible });

  handleSearchChange = e =>
    this.setState({ [e.target.name]: e.target.value, currentPage: 1 });

  handlePageChange = page => this.setState({ currentPage: page });

  handleProductDetails = invoice => this.setState({ invoice });

  handleSort = column => {
    var sortColumn = { column };
    if (column === this.state.sortColumn.column)
      sortColumn.order = this.state.sortColumn.order === "asc" ? "desc" : "asc";
    else {
      sortColumn.order = "asc";
    }
    this.setState({ sortColumn, currentPage: 1 });
  };

  renderSortIcon = column => {
    const { sortColumn } = this.state;

    if (column !== sortColumn.column) return <Icon name="sort" />;
    if (sortColumn.order === "asc") return <Icon name="sort up" />;
    return <Icon name="sort down" />;
  };

  render() {
    const {
      visible,
      searchTerm,
      currentPage,
      pageSize,
      sortColumn,
      invoice
    } = this.state;

    const { invoices: allInvoices } = this.state;

    const invoicesData =
      searchTerm !== ""
        ? allInvoices.filter(invoice => invoice.name.startsWith(searchTerm))
        : allInvoices;
    const totalCount = invoicesData.length;

    const sortedInvoices = _.orderBy(
      invoicesData,
      [sortColumn.column],
      [sortColumn.order]
    );
    const invoices = paginate(sortedInvoices, currentPage, pageSize);

    return (
      <Grid divided="vertically">
        <Grid.Row columns={1}>
          <Grid.Column>
            <Segment clearing>
              <Header
                fluid="true"
                as="h2"
                floated="left"
                style={{ marginBottom: 0 }}
              >
                <span>
                  Invoices
                  <Icon name="product hunt" color="black" />
                </span>
                <Header.Subheader>
                  {allInvoices.length} Invoices
                </Header.Subheader>
              </Header>
              <Header floated="right">
                <Input
                  // loading={searchLoading}
                  onChange={this.handleSearchChange}
                  size="mini"
                  icon="search"
                  name="searchTerm"
                  placeholder="Search Messages"
                />
              </Header>
              <Button.Group>
                <Button onClick={this.handleAnimationChange()}>
                  Show sidebar
                </Button>
                <Button>
                  <Link to="invoiceForm">New Invoice</Link>
                </Button>
                <Button>Export</Button>
              </Button.Group>
            </Segment>

            <Sidebar.Pushable style={{ height: "60vh" }}>
              <Sidebar
                as={Grid}
                direction="right"
                animation="overlay"
                icon="labeled"
                vertical
                visible={visible}
                style={{ width: "50%", paddingRight: 0 }}
              >
                <Grid
                  style={{
                    background: "#fff",
                    width: "100%",
                    marginLeft: 0
                  }}
                  textAlign="center"
                  verticalAlign="middle"
                >
                  <Grid.Column fluid>
                    {!invoice && (
                      <Icon
                        name="file alternate outline"
                        size="huge"
                        bordered
                      />
                    )}
                    {invoice && (
                      <React.Fragment>
                        <Icon
                          name="file alternate"
                          size="big"
                          color="black"
                          circular
                        />
                        <Header as="h1" textAlign="center">
                          {invoice.number}
                        </Header>
                        <Header sub>
                          Number : <span> {invoice.number}</span>
                        </Header>
                        <Header sub>
                          Customer : <span> {invoice.cutomer}</span>
                        </Header>
                        <Header sub>
                          Created : <span> {invoice.created}</span>
                        </Header>
                        <Header sub>
                          Due Date : <span> {invoice.dueDate}</span>
                        </Header>
                        <Header sub>
                          Paid : <span> {invoice.dueDate}</span>
                        </Header>
                        <Header sub>
                          Total : <span> {invoice.total}</span>
                        </Header>
                        <Header sub>
                          Status : <span> {invoice.status}</span>
                        </Header>
                      </React.Fragment>
                    )}
                  </Grid.Column>
                </Grid>
              </Sidebar>

              <Sidebar.Pusher>
                <Table celled style={{ width: visible ? "50%" : "100%" }}>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("number")}
                        >
                          Number {this.renderSortIcon("number")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("cutomer")}
                        >
                          Customer {this.renderSortIcon("cutomer")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("created")}
                        >
                          Created {this.renderSortIcon("created")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("dueDate")}
                        >
                          Due Date {this.renderSortIcon("dueDate")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("paid")}
                        >
                          Paid {this.renderSortIcon("paid")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("total")}
                        >
                          Total {this.renderSortIcon("total")}
                        </label>
                      </Table.HeaderCell>
                      <Table.HeaderCell>
                        <label
                          className="pointer"
                          onClick={() => this.handleSort("status")}
                        >
                          Status {this.renderSortIcon("status")}
                        </label>
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>

                  <Table.Body>
                    {invoices.map(invoice => (
                      <Table.Row
                        key={invoice._id}
                        onClick={() => this.handleProductDetails(invoice)}
                      >
                        <Table.Cell>{invoice.number}</Table.Cell>
                        <Table.Cell>{invoice.cutomer}</Table.Cell>
                        <Table.Cell>{invoice.created}</Table.Cell>
                        <Table.Cell>{invoice.dueDate}</Table.Cell>
                        <Table.Cell>{invoice.paid}</Table.Cell>
                        <Table.Cell>{invoice.total}</Table.Cell>
                        <Table.Cell>{invoice.status}</Table.Cell>
                      </Table.Row>
                    ))}
                  </Table.Body>

                  <Table.Footer>
                    <Table.Row>
                      <Table.HeaderCell colSpan={visible ? 7 : 7}>
                        <Pagination
                          itemsCount={totalCount}
                          pageSize={pageSize}
                          currentPage={currentPage}
                          onPageChange={this.handlePageChange}
                        />
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Footer>
                </Table>
              </Sidebar.Pusher>
            </Sidebar.Pushable>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapStateToProps = ({ product }) => {
  const { products } = product;
  return { products };
};

export default connect(
  mapStateToProps,
  { getProducts }
)(Invoices);
