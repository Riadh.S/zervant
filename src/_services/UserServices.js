import { Service } from "./Service";
import Axios from "axios";
import jwtDecode from "jwt-decode";
const headers = {
  "x-access-token": localStorage.getItem("token")
};
export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem("token");
    const resultat = jwtDecode(jwt);
    const email = resultat.email;
    const indexAt = email.indexOf("@");
    const user = email.slice(0, indexAt);
    return user;
  } catch (err) {
    return null;
  }
}
export function getIdUser() {
  try {
    const jwt = localStorage.getItem("token");
    const resultat = jwtDecode(jwt);
    return resultat.user_id;
  } catch (err) {
    return null;
  }
}
export function getUser(id) {
  let apiEndpoint = "users/" + id;
  Service.get(apiEndpoint)
    .then(response => {
      console.log(response.data.data, "responsssse");
      return response.data.data;
    })
    .catch(err => console.log(err));
}
