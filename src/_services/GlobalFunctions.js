export function changeFormatDate(date) {
  console.log(date);
  let dd = date.getDate();
  let mm = date.getMonth() + 1;
  let y = date.getFullYear();
  var FormattedDate = mm + "-" + dd + "-" + y;
  return FormattedDate;
}
export function changeFormatTimeStamp(date) {
  let str = `${date}`;
  let newDate = str.substring(0, 10);
  return newDate;
}
export function calculateTTC(data) {
  let vat = 1 + data.vat / 100;
  console.log(vat);
  let priceTTC = data.priceWT * vat;
  if (!priceTTC) return;
  return priceTTC.toFixed(2);
}
