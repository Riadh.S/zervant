import React, { Component } from "react";
import { Switch, Route, Redirect, withRouter, Router } from "react-router-dom";
import { IntlProvider } from "react-intl";
import { connect } from "react-redux";
import Register from "./components/Auth/Register";
import Login from "./components/Auth/Login";
import Spinner from "./components/Spinner";
import Home from "./components/Home/Home";
import Navbar from "./components/Navbar/Navbar";
import Products from "./components/Products/Products";
import Customers from "./components/Customers/Customers";
import Invoices from "./components/Invoices/Invoices";
import InvoiceForm from "./components/Invoices/InvoiceForm";
import messages from "./translations";
import "semantic-ui-css/semantic.min.css";
import { Grid } from "semantic-ui-react";
import { setUser, clearUser } from "./_actions";
import { PrivateRoute } from "./components/PrivateRoute";
import { history } from "./_helpers";

class App extends Component {
  componentDidMount() {
    if (localStorage.token && localStorage.auth) {
      this.props.setUser(localStorage.token, localStorage.auth);
      history.push("/home");
    } else {
      this.props.clearUser();
      history.push("/login");
    }
  }

  render() {
    const { lang } = this.props;

    return (
      <IntlProvider
        locale={lang}
        messages={messages[lang]}
        textComponent={React.Fragment}
      >
        {this.props.isLoading ? (
          <Spinner />
        ) : (
          <Router history={history}>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route path="/login" component={Login} />
              <Route path="/register" component={Register} />
              <Grid
                columns="equal"
                className="app"
                style={{ background: "#eee" }}
              >
                <Navbar />
                <Grid.Column style={{ marginLeft: 320 }}>
                  <PrivateRoute path="/home" component={Home} />
                  <PrivateRoute path="/products" component={Products} />
                  <PrivateRoute path="/customers" component={Customers} />
                  <PrivateRoute path="/invoices" component={Invoices} />
                  <PrivateRoute path="/invoiceForm" component={InvoiceForm} />
                </Grid.Column>
              </Grid>
            </Switch>
          </Router>
        )}
      </IntlProvider>
    );
  }
}

const mapStateToProps = ({ locale, authentication }) => {
  const { lang } = locale;
  const { isLoading } = authentication;
  return { lang, isLoading };
};

export default withRouter(
  connect(
    mapStateToProps,
    { setUser, clearUser }
  )(App)
);
