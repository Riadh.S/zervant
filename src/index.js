import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import de from "react-intl/locale-data/de";
import reducer from "./_reducers";
import { setLocale } from "./_actions";
import "semantic-ui-css/semantic.min.css";
import { composeWithDevTools } from "redux-devtools-extension";

addLocaleData(en);
addLocaleData(de);

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

if (localStorage.lang) {
  store.dispatch(setLocale(localStorage.lang));
}

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
