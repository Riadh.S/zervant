import React, { Component } from "react";
import Joi from "joi";
import Input from "./Input";
import RadioBox from "./RadioBox";
import { Button, Icon } from "semantic-ui-react";

class JoiForm extends Component {
  state = {
    data: {},
    errors: {}
  };
  validate = () => {
    const { error } = Joi.validate(this.state.data, this.schema, {
      abortEarly: false
    });

    if (!error) return null;
    const errors = {};
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };
  handleSubmit = e => {
    e.preventDefault();
    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;
    this.doSubmit();
  };

  handleChange = e => {
    const data = { ...this.state.data };
    data[e.target.name] = e.target.value;
    this.setState({ data });
  };

  customHandleChange = (e, event) => {
    const data = { ...this.state.data };
    data[event.name] = event.value;
    this.setState({ data });
  };

  inputForm(
    label,
    name,
    placeholder,
    type = "text",
    required,
    icon,
    iconPosition
  ) {
    console.log("inputForm", this.state);

    const { data, errors } = this.state;
    return (
      <Input
        label={label}
        name={name}
        type={type}
        value={data[name]}
        error={errors[name]}
        onHandleChange={this.handleChange}
        placeholder={placeholder}
        required={required}
        icon={icon}
        iconPosition={iconPosition}
      />
    );
  }

  radioBoxForm(value, name, label) {
    return (
      <RadioBox
        value={value}
        name={name}
        label={label}
        onHandleChange={() => this.handleCheck(value, name)}
      />
    );
  }

  buttonSubmit(label) {
    return (
      <Button color="green" size="large" inverted floated="right">
        <Icon name="save" />
        {label}
      </Button>
    );
  }
}

export default JoiForm;
