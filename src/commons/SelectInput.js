import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

const SelectInput = props => {
  const { label, name, value } = props;
  return (
    <div className="form-inline">
      <div className="col-2" />
      <div className="col-2">
        <label>{label}</label>
      </div>
      <div className="col-5">
        <select
          style={{ width: "60%" }}
          className="form-control"
          name={name}
          onChange={e => props.onHandleSelect(e)}
        >
          {value.map((genre, key) => (
            <option key={key} value={genre}>
              {genre}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default SelectInput;
