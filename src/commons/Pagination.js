import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { Menu, Icon } from "semantic-ui-react";

const Pagination = ({ itemsCount, pageSize, currentPage, onPageChange }) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);
  if (pagesCount === 1) return null;
  const pages = _.range(1, pagesCount + 1);

  return (
    <Menu floated="right" pagination>
      {currentPage > 1 ? (
        <Menu.Item as="a" icon onClick={() => onPageChange(currentPage - 1)}>
          <Icon name="chevron left" />
        </Menu.Item>
      ) : null}

      {pages.map(page => (
        <Menu.Item
          key={page}
          as="a"
          className={page === currentPage ? "active" : ""}
          onClick={() => onPageChange(page)}
        >
          {page}
        </Menu.Item>
      ))}
      {currentPage < Math.ceil(itemsCount / pageSize) ? (
        <Menu.Item as="a" icon onClick={() => onPageChange(currentPage + 1)}>
          <Icon name="chevron right" />
        </Menu.Item>
      ) : null}
    </Menu>
  );
};

Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default Pagination;
