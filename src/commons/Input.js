import React, { Component } from "react";
import { Form, Message } from "semantic-ui-react";
class Input extends Component {
  render() {
    const {
      value,
      type,
      name,
      label,
      error,
      placeholder,
      required,
      icon,
      iconPosition
    } = this.props;
    return (
      <React.Fragment>
        <Form.Field>
          <label>
            {label}
            {required ? " *" : ""}
          </label>
          <Form.Input
            placeholder={placeholder}
            iconPosition={iconPosition}
            icon={icon}
            fluid
            type={type}
            name={name}
            value={value}
            id={name}
            onChange={e => this.props.onHandleChange(e)}
          />
          {error && (
            <Message style={{ marginTop: "-1em" }} error content={error} />
          )}
        </Form.Field>
      </React.Fragment>
    );
  }
}
export default Input;
