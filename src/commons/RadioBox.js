import React from "react";
const RadioBox = props => {
  const { name, value, label } = props;
  return (
    <div className="form-inline">
      {/* <div className="col-6" /> */}
      <div className="col-6 my-2">
        <label>{label}</label>
      </div>

      <input
        type="radio"
        checked={value}
        name={name}
        onChange={() => props.onHandleChange({ value }, { name })}
      />
    </div>
    // <div className="custom-control custom-radio">
    //   <input
    //     type="radio"
    //     checked={value}
    //     name={name}
    //     id={name}
    //     className="custom-control-input"
    //     onChange={() => props.onHandleChange({ value }, { name })}
    //   />
    //   <label className="custom-control-label" for={name}>
    //     {label}
    //   </label>
    // </div>
  );
};

export default RadioBox;
